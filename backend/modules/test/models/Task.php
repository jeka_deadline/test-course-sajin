<?php

namespace backend\modules\test\models;

use Yii;
use common\models\test\Task as BaseTask;
use yii\helpers\ArrayHelper;

class Task extends BaseTask
{

    public function getFormElements()
    {
        return [
            'section_id'    => ['type' => 'dropdownlist', 'items' => ArrayHelper::map(Section::find()->all(), 'id', 'name'), 'attributes' => ['prompt' => 'Выберите главу']],
            'part_id'       => ['type' => 'dropdownlist', 'items' => ArrayHelper::map(Part::find()->all(), 'id', 'title'), 'attributes' => ['prompt' => 'Выберите раздел']],
            'title'         => ['type' => 'textInput'],
            'text'          => [
                                  'type' => 'widget', 'nameWidget' => '\zxbodya\yii2\tinymce\TinyMce',
                                  'attributes' =>  [
                                      'options' => ['rows' => '10'],
                                      'language' => 'ru',
                                      'settings' => [
                                          'forced_root_block' => FALSE,
                                      ],
                                      'fileManager' => [
                                          'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
                                          'connectorRoute' => 'task/connector',
                                      ],
                                  ]
                              ],
            'display_order' => ['type' => 'textInput'],
            'active'        => ['type' => 'checkbox'],
        ];
    }

    public function getViewAttributes()
    {
        return ['id', 'section_id', 'title', 'display_order', 'active'];
    }

    public function getSection()
    {
        return $this->hasOne(Section::className(), ['id' => 'section_id']);
    }

    public function getPart()
    {
        return $this->hasOne(Part::className(), ['id' => 'part_id']);
    }

}
