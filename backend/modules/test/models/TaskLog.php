<?php

namespace backend\modules\test\models;

use Yii;
use common\models\test\TaskLog as BaseTaskLog;

class TaskLog extends BaseTaskLog
{

    public function getFormElements()
    {
        return [
            'status'      => ['type' => 'dropdownlist', 'items' => $this->getListStatus()],
            'text_answer' => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
        ];
    }

    public function getViewAttributes()
    {
        return ['id', 'user_id', 'task_id', 'status'];
    }

    public function getListStatus()
    {
        return [
            'new'     => 'Новая',
            'correct' => 'Правильный',
            'wrong'   => 'Неправильный',
            'rework'  => 'Требует доработки',
        ];
    }

}
