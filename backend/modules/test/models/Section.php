<?php

namespace backend\modules\test\models;

use Yii;
use common\models\test\Section as BaseSection;

class Section extends BaseSection
{

    public function getFormElements()
    {
        return [
            'name'          => ['type' => 'textInput'],
            'title_time'    => ['type' => 'textarea', 'attributes' => ['rows' => 2]],
            'display_order' => ['type' => 'textInput'],
            'active'        => ['type' => 'checkbox'],
        ];
    }

    public function getViewAttributes()
    {
        return ['id', 'name', 'display_order'];
    }

}
