<?php
namespace backend\modules\test\models;

use Yii;
use common\models\test\Test as BaseTest;
use yii\helpers\ArrayHelper;


class Test extends BaseTest
{

    public function getFormElements()
    {
        return [
            'section_id'    => ['type' => 'dropdownlist', 'items' => ArrayHelper::map(Section::find()->all(), 'id', 'name')],
            'name'          => ['type' => 'textInput'],
            'description'   => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
            'display_order' => ['type' => 'textInput'],
            'active'        => ['type' => 'checkbox'],
        ];
    }

    public function getViewAttributes()
    {
        return ['id', 'name', 'description', 'display_order'];
    }

    public function getSection()
    {
        return $this->hasOne(Section::className(), ['id' => 'section_id']);
    }

}
