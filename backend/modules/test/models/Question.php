<?php

namespace backend\modules\test\models;

use Yii;
use common\models\test\Question as BaseQuestion;
use yii\helpers\ArrayHelper;

class Question extends BaseQuestion
{
    public $tests_list;

    public function getFormElements()
    {
        $this->tests_list = ArrayHelper::map(LinksTestsQuestions::find()->where(['question_id' => $this->id])->all(), 'id', 'test_id');

        return [
            'type'            => ['type' => 'dropdownlist', 'items' => $this->getListQuestionsType()],
            'name'            => ['type' => 'textInput'],
            'title'           => [
                                      'type' => 'widget', 'nameWidget' => '\zxbodya\yii2\tinymce\TinyMce',
                                      'attributes' =>  [
                                          'options' => ['rows' => '10'],
                                          'language' => 'ru',
                                          'settings' => [
                                              'forced_root_block' => FALSE,
                                          ],
                                          'fileManager' => [
                                              'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
                                              'connectorRoute' => 'question/connector',
                                          ],
                                      ]
                                  ],
            'correct_answer'  => ['type' => 'textarea', 'attributes' => ['rows' => 5]],
            'display_order'   => ['type' => 'textInput'],
            'tests_list'      => ['type' => 'checkboxlist', 'items' => ArrayHelper::map(Test::find()->all(), 'id', 'name')],
            'active'          => ['type' => 'checkbox'],
        ];
    }

    public function getViewAttributes()
    {
        return ['id', 'name', 'title', 'display_order'];
    }

    public function getListQuestionsType()
    {
        return [
            'one' => 'Один вариант ответа',
        ];
    }

    public function afterSave($insert, $changesAttributes)
    {
        $listLinks = ArrayHelper::map(LinksTestsQuestions::find()->where(['question_id' => $this->id])->all(), 'test_id', function($model){ return $model; });

        if (isset($_POST[ 'Question' ][ 'tests_list' ]) && !empty($_POST[ 'Question' ][ 'tests_list' ])) {
            foreach ($_POST[ 'Question' ][ 'tests_list' ] as $testId) {

                if (!isset($listLinks[ $testId ])) {
                    
                    $model              = new LinksTestsQuestions();
                    $model->question_id = $this->id;
                    $model->test_id     = $testId;
                    
                    $model->save(FALSE);
                
                } else {
                    //throw new \Exception($testId);
                    
                    unset($listLinks[ $testId ]);
                }

            }
        }

        foreach ($listLinks as $model) {
            $model->delete();
        }

        return parent::afterSave($insert, $changesAttributes);
    }

}
