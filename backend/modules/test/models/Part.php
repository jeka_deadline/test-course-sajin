<?php

namespace backend\modules\test\models;

use Yii;
use common\models\test\Part as BasePart;
use yii\helpers\ArrayHelper;


class Part extends BasePart
{

    public function getFormElements()
    {
        return [
            'section_id'    => ['type' => 'dropdownlist', 'items' => ArrayHelper::map(Section::find()->all(), 'id', 'name')],
            'title'         => ['type' => 'textInput'],
            'text'          => [
                                  'type' => 'widget', 'nameWidget' => '\zxbodya\yii2\tinymce\TinyMce',
                                  'attributes' =>  [
                                      'options' => ['rows' => '10'],
                                      'language' => 'ru',
                                      'settings' => [
                                          'forced_root_block' => FALSE,
                                      ],
                                      'fileManager' => [
                                          'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
                                          'connectorRoute' => 'part/connector',
                                      ],
                                  ]
                              ],
            'display_order' => ['type' => 'textInput'],
            'active'        => ['type' => 'checkbox'],
        ];
    }

    public function getViewAttributes()
    {
        return ['id', 'section_id', 'title', 'display_order', 'active'];
    }

    public function getSection()
    {
        return $this->hasOne(Section::className(), ['id' => 'section_id']);
    }

}
