<?php

namespace backend\modules\test\models;

use Yii;
use common\models\test\Answer as BaseAnswer;
use yii\helpers\ArrayHelper;

class Answer extends BaseAnswer
{

    public function getFormElements()
    {
        return [
            'question_id'   => ['type' => 'dropdownlist', 'items' => ArrayHelper::map(Question::find()->all(), 'id', 'name'), 'attributes' => ['prompt' => 'Выберите вопрос']],
            'type'          => ['type' => 'dropdownlist', 'items' => $this->getListAnswerTypes()],
            'text'          => [
                                  'type' => 'widget', 'nameWidget' => '\zxbodya\yii2\tinymce\TinyMce',
                                  'attributes' =>  [
                                      'options' => ['rows' => '10'],
                                      'language' => 'ru',
                                      'settings' => [
                                          'forced_root_block' => FALSE,
                                      ],
                                      'fileManager' => [
                                          'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
                                          'connectorRoute' => 'answer/connector',
                                      ],
                                  ]
                              ],
            'display_order' => ['type' => 'textInput'],
            'active'        => ['type' => 'checkbox'],
        ];
    }

    public function getViewAttributes()
    {
        return ['id', 'question_id', 'type', 'display_order'];
    }

    public function getListAnswerTypes()
    {
        return [
            'correct' => 'Правильный',
            'wrong'   => 'Неправильный',
        ];
    }

}
