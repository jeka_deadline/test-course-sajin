<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\modules\user\models\User;
?>

<h2>Лог заданий</h2>

<div>

    <?= Html::beginForm(Url::toRoute(['/test/task/clear-tasks'])); ?>

        <?= Html::dropDownList('user_id', NULL, ArrayHelper::map(User::find()->all(), 'id', function($model){ return $model->surname . ' ' . $model->name;}), ['class' => 'form-control', 'prompt' => 'Выберите пользователя']); ?>

        <br>

        <?= Html::submitInput('Очистить задания пользователя', ['class' => 'btn btn-danger']); ?>

    <?= Html::endForm(); ?>

<br>
</div>

<?= GridView::widget([
    'dataProvider'  => $dataProvider,
    'filterModel'   => $searchModel,
    'columns'       => $columns,
]); ?>