<?php
namespace backend\modules\test;

use Yii;

class Module extends \yii\base\Module
{

    public $controllerNamespace = 'backend\modules\test\controllers';

    public function init()
    {
        parent::init();
    }

}