<?php
namespace backend\modules\test\controllers;

use backend\modules\core\components\BackendController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class SectionController extends BackendController
{
    public $modelName   = '\backend\modules\test\models\Section';
    public $searchModel = '\backend\modules\test\models\searchModels\SectionSearch';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class'           => 'backend\modules\core\components\CRUDIndex',
                'title'           => 'Главы',
            ],
            'create' => [
                'class'           => 'backend\modules\core\components\CRUDCreate',
                'title'           => 'Добавить главу',
                'modelName'       => $this->modelName,
            ],
            'update' => [
                'class'           => 'backend\modules\core\components\CRUDUpdate',
                'title'           => 'Обновить главу',
                'modelName'       => $this->modelName,
            ],
            'view' => [
                'class'           => 'backend\modules\core\components\CRUDView',
                'title'           => 'Просмотр главы',
                'modelName'       => $this->modelName,
            ],
            'delete' => [
                'class'           => 'backend\modules\core\components\CRUDDelete',
                'modelName'       => $this->modelName,
            ],
        ];
    }

    public function getColumns()
    {
        return [
            $this->getGridSerialColumn(),
            ['attribute'  => 'name'],
            ['attribute'  => 'display_order'],
            $this->getGridActive(),
            $this->getGridActions(),

        ];
    }
}
?>