<?php
namespace backend\modules\test\controllers;

use Yii;
use backend\modules\core\components\BackendController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use zxbodya\yii2\elfinder\ConnectorAction;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use backend\modules\test\models\Section;

class PartController extends BackendController
{
    public $modelName   = '\backend\modules\test\models\Part';
    public $searchModel = '\backend\modules\test\models\searchModels\PartSearch';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view', 'connector'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class'           => 'backend\modules\core\components\CRUDIndex',
                'title'           => 'Разделы',
            ],
            'create' => [
                'class'           => 'backend\modules\core\components\CRUDCreate',
                'title'           => 'Добавить раздел',
                'modelName'       => $this->modelName,
            ],
            'update' => [
                'class'           => 'backend\modules\core\components\CRUDUpdate',
                'title'           => 'Обновить раздел',
                'modelName'       => $this->modelName,
            ],
            'view' => [
                'class'           => 'backend\modules\core\components\CRUDView',
                'title'           => 'Просмотр раздела',
                'modelName'       => $this->modelName,
            ],
            'delete' => [
                'class'           => 'backend\modules\core\components\CRUDDelete',
                'modelName'       => $this->modelName,
            ],
            'connector' => array(
                'class' => ConnectorAction::className(),
                'settings' => array(
                    'root' => Yii::getAlias('@frontend/web/') . $this->model->filePath,
                    'URL' => Yii::$app->params[ 'basePath' ] . '/frontend/web/' . $this->model->filePath . '/',
                    'rootAlias' => 'Home',
                    'mimeDetect' => 'none'
                )
            ),
        ];
    }

    public function getColumns()
    {
        return [
            $this->getGridSerialColumn(),
            [
                'attribute'  => 'section_id',
                'value'       => function($model){return $model->section->name;},
                'filter' => Html::activeDropDownList($this->model, 'section_id', ArrayHelper::map(Section::find()->all(), 'id', 'name'), ['class' => 'form-control', 'prompt' => '']),
            ],
            ['attribute'  => 'title'],
            ['attribute'  => 'display_order'],
            $this->getGridActive(),
            $this->getGridActions(),

        ];
    }
}
?>