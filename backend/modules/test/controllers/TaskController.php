<?php
namespace backend\modules\test\controllers;

use Yii;
use backend\modules\core\components\BackendController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use zxbodya\yii2\elfinder\ConnectorAction;
use yii\helpers\Url;
use backend\modules\test\models\TaskLog;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use backend\modules\test\models\Section;
use backend\modules\test\models\Part;

class TaskController extends BackendController
{
    public $modelName   = '\backend\modules\test\models\Task';
    public $searchModel = '\backend\modules\test\models\searchModels\TaskSearch';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view', 'connector', 'clear-tasks'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class'           => 'backend\modules\core\components\CRUDIndex',
                'title'           => 'Задания',
            ],
            'create' => [
                'class'           => 'backend\modules\core\components\CRUDCreate',
                'title'           => 'Добавить задание',
                'modelName'       => $this->modelName,
            ],
            'update' => [
                'class'           => 'backend\modules\core\components\CRUDUpdate',
                'title'           => 'Обновить задание',
                'modelName'       => $this->modelName,
            ],
            'view' => [
                'class'           => 'backend\modules\core\components\CRUDView',
                'title'           => 'Просмотр заданий',
                'modelName'       => $this->modelName,
            ],
            'delete' => [
                'class'           => 'backend\modules\core\components\CRUDDelete',
                'modelName'       => $this->modelName,
            ],
            'connector' => array(
                'class' => ConnectorAction::className(),
                'settings' => array(
                    'root' => Yii::getAlias('@frontend/web/') . $this->model->filePath,
                    'URL' => Yii::$app->params[ 'basePath' ] . '/frontend/web/' . $this->model->filePath . '/',
                    'rootAlias' => 'Home',
                    'mimeDetect' => 'none'
                )
            ),
        ];
    }

    public function getColumns()
    {
        return [
            $this->getGridSerialColumn(),
            [
                'attribute'  => 'section_id',
                'value'       => function($model){return ($model->section) ? $model->section->name : NULL;},
                'filter' => Html::activeDropDownList($this->model, 'section_id', ArrayHelper::map(Section::find()->all(), 'id', 'name'), ['class' => 'form-control', 'prompt' => '']),
            ],
            [
                'attribute'  => 'part_id',
                'value'       => function($model){return ($model->part) ? $model->part->title : NULL;},
                'filter' => Html::activeDropDownList($this->model, 'part_id', ArrayHelper::map(Part::find()->all(), 'id', 'title'), ['class' => 'form-control', 'prompt' => '']),
            ],
            ['attribute'  => 'title'],
            ['attribute'  => 'display_order'],
            $this->getGridActive(),
            $this->getGridActions(),

        ];
    }

    public function actionClearTasks()
    {
        $tasks = TaskLog::find()
                            ->where(['user_id' => Yii::$app->request->post('user_id')])
                            ->all();

        foreach ($tasks as $task) {
            $task->delete();
        }

        return $this->redirect(['/test/task-log/index']);
    }
}
?>