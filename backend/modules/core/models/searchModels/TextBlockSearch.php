<?php

namespace backend\modules\core\models\searchModels;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\core\models\TextBlock;

/**
 * TextBlockSearch represents the model behind the search form about `common\models\photo\TextBlock`.
 */
class TextBlockSearch extends TextBlock
{

    public function rules()
    {
        return [
            [['id', 'display_order', 'active'], 'integer'],
            [['title', 'description', 'uri', 'content'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = TextBlock::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'              => $this->id,
            'display_order'   => $this->display_order,
            'active'          => $this->active,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'uri', $this->uri]);

        return $dataProvider;
    }
}
