<?php

namespace backend\modules\core\models;

use Yii;
use common\models\core\TextBlock as BaseTextBlock;
use yii\helpers\ArrayHelper;

class TextBlock extends BaseTextBlock
{

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['display_order'], 'default', 'value' => 0],
            ]
        );
    }

    public function getFormElements()
    {
        return [
            'title'         => ['type' => 'textInput'],
            'uri'           => ['type' => 'textInput'],
            'description'   => ['type' => 'textarea', 'attributes' => ['rows' => 5]],
            'content'       => [
                                  'type' => 'widget', 'nameWidget' => '\zxbodya\yii2\tinymce\TinyMce',
                                  'attributes' =>  [
                                      'options' => ['rows' => '10'],
                                      'language' => 'ru',
                                      'settings' => [
                                          'forced_root_block' => FALSE,
                                      ],
                                      'fileManager' => [
                                          'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
                                          'connectorRoute' => 'text-block/connector',
                                      ],
                                  ]
                              ],
            'uri'           => ['type' => 'textInput'],
            'display_order' => ['type' => 'textInput'],
            'active'        => ['type' => 'checkbox'],
        ];
    }

    public function getViewAttributes()
    {
        return ['id', 'title', 'uri', 'description', 'content', 'display_order', 'active'];
    }
}
