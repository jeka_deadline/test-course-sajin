<?php
namespace backend\modules\core;

use Yii;

class Module extends \yii\base\Module
{

    public $controllerNamespace = 'backend\modules\core\controllers';

    public function init()
    {
        parent::init();
    }

}