<?php
namespace backend\modules\user\models;

use Yii;
use common\models\user\User as BaseUser;

class User extends BaseUser
{

    const ROLE_USER   = '2';
    const ROLE_ADMIN  = '1';

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

}
?>