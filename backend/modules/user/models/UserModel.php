<?php
namespace backend\modules\user\models;

use yii\base\Model;

class UserModel extends Model
{

    public $surname;
    public $name;
    public $password;
    public $role;
    public $blocked;
    public $sectionId;
    public $partId;
    public $status;

    public function rules()
    {
        return [
            [['surname', 'name', 'password'], 'string', 'max' => 255],
            [['sectionId', 'partId', 'role', 'blocked'], 'integer'],
            [['status'], 'string'],
        ];
    }

    public function getListRoles()
    {
        return [
            User::ROLE_ADMIN  => 'Администратор',
            User::ROLE_USER   => 'Пользователь',
        ];
    }

    public function getListStatus()
    {
        return [
            'start' => 'Начинает обучение',
            'finish'  => 'Итог',
            'conclusion' => 'Заключение',
            'end' => 'Закончил обучение',
        ];
    }

}
?>