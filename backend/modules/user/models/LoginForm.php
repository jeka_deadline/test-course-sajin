<?php
namespace backend\modules\user\models;

use Yii;

class LoginForm extends \yii\base\Model
{

    public $login;
    public $password;

    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
        ];
    }

    public function login()
    {
        $user = User::find()
                          ->where(['login' => $this->login])
                          ->andWhere(['blocked' => '0'])
                          ->one();
        
        if ($user && $user->validatePassword($this->password)) {
            Yii::$app->user->login($user);
            return TRUE;
        }

        return FALSE;
    }

}