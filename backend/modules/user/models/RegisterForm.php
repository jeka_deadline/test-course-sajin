<?php
namespace backend\modules\user\models;

use Yii;

class RegisterForm extends \yii\base\Model
{

    public $email;
    public $login;
    public $password_hash;
    public $role;
    public $surname;
    public $name;

    public function rules()
    {
        return [
            [['email', 'login', 'password_hash', 'role', 'surname', 'name'], 'required'],
            [['email'], 'unique', 'targetClass' => User::className()],
            [['login'], 'unique', 'targetClass' => User::className()],
            [['email'], 'email'],
            [['email'], 'string', 'max' => 100],
            [['login'], 'string', 'max' => 50],
            [['password_hash', 'surname', 'name'], 'string', 'max' => 255],
        ];
    }

    public function getListRoles()
    {
        return [
            User::ROLE_USER   => 'Пользователь',
            User::ROLE_ADMIN  => 'Администратор',
        ];
    }

    public function register()
    {
        if ($this->validate()) {
            $model          = new User();
            $model->email   = $this->email;
            $model->login   = $this->login;
            $model->role    = $this->role;
            $model->surname = $this->surname;
            $model->name    = $this->name;

            $model->setPassword($this->password_hash);

            if ($model->validate() && $model->save()) {
                return TRUE;
            }
        }

        return FALSE;
    }

}
?>