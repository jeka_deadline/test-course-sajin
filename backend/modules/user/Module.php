<?php
namespace backend\modules\user;

use Yii;

class Module extends \yii\base\Module
{

    public $controllerNamespace = 'backend\modules\user\controllers';

    public function init()
    {
        parent::init();
    }

}