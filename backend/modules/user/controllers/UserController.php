<?php
namespace backend\modules\user\controllers;

use Yii;
use backend\modules\core\components\BackendController;
use backend\modules\user\models\UserModel;
use yii\helpers\Url;
use backend\modules\test\models\Section;
use backend\modules\test\models\Part;
use backend\modules\test\models\Task;
use backend\modules\test\models\TaskLog;
use yii\web\Response;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use backend\modules\user\models\LearnStatus;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use backend\modules\user\models\LearnTime;
use common\models\test\LogTest;

class UserController extends BackendController
{

    public $searchModel = '\backend\modules\user\models\searchModels\UserSearch';
    public $modelName   = '\backend\modules\user\models\User';

    public function actions()
    {
        return [
            'delete' => [
                'class'           => 'backend\modules\core\components\CRUDDelete',
                'modelName'       => $this->modelName,
            ],
        ];
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'delete', 'get-list-parts', 'reset'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new $this->searchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getColumns(),
        ]);
    }

    public function actionUpdate($id)
    {
        $user = call_user_func([$this->modelName, 'findOne'], [$id]);

        if (!$user) {

        }

        $userActive = LearnStatus::find()->where(['user_id' => $user->id])->one();

        $model = new UserModel();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user->surname  = $model->surname;
            $user->name     = $model->name;
            
            if ($model->password) {
                $user->setPassword($model->password);
            }

            if ($user->id !== Yii::$app->user->identity->getId()) {
                $user->blocked = ($model->blocked) ? time() : NULL;
                $user->role     = $model->role;
            }

            $user->save();

            if ($userActive) {
                $userActive->section_id = $model->sectionId;
                $userActive->part_id = $model->partId;
                $userActive->status = ($model->status) ? $model->status : NULL;
                $userActive->save();              
            }

            return $this->redirect(Url::toRoute(['/user/user/index']));
        }

        $model->surname = $user->surname;
        $model->name    = $user->name;
        $model->blocked = ($user->blocked) ? '1' : '0';
        $model->role    = $user->role;

        if ($userActive) {

            $model->sectionId = $userActive->section_id;
            $model->partId    = $userActive->part_id;
            $model->status    = $userActive->status;

        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function getColumns()
    {
        return [
            $this->getGridSerialColumn(),
            ['attribute'  => 'email'],
            ['attribute'  => 'surname'],
            ['attribute'  => 'name'],
            $this->getGridActions(),

        ];
    }

    public function actionGetListParts()
    {     
          $sectionId = Yii::$app->request->post('sectionId');
          

          Yii::$app->response->format = Response::FORMAT_JSON;

          $model = new UserModel();

          return Html::activeDropDownList($model, 'partId', ArrayHelper::map(Part::find()->where(['section_id' => $sectionId])->all(), 'id', 'title'), ['prompt' => 'Выберите раздел', 'class' => 'form-control']);
    }

    public function actionReset($id)
    {
        $model = LearnStatus::find()->where(['user_id' => $id])->one();

        if (!$model) {
            $user = call_user_func([$this->modelName, 'findOne'], [$id]);

            if (!$user) {
                return FALSE;
            }

            $model = new LearnStatus();
            $model->user_id = $user->id;
        }

        $model->part_id         = NULL;
        $model->section_id      = NULL;
        $model->test_id         = NULL;
        $model->task_section_id = NULL;
        $model->task_part_id    = NULL;
        $model->block_route     = NULL;
        $model->block_data      = NULL;
        $model->status          = 'start';
    
        $model->save();

        $tasksLog = TaskLog::find()
                                ->where(['user_id' => $id])
                                ->all();

        foreach ($tasksLog as $task) {
            $task->delete();
        }

        $learnTime = LearnTime::find()
                                    ->where(['user_id' => $id])
                                    ->all();

        foreach ($learnTime as $time) {
            $time->delete();
        }

        $logTest = LogTest::find()
                                ->where(['user_id' => $id])
                                ->all();

        foreach ($logTest as $test) {
            $test->delete();
        }

        return $this->render('success-reset');

    }

}
?>