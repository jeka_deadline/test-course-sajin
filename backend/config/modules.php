<?php
    return [
        'core' => [
            'class' => 'backend\modules\core\Module',
        ],
        'user' => [
            'class' => 'backend\modules\user\Module',
        ],
        'test' => [
            'class' => 'backend\modules\test\Module',
        ],
    ];
?>