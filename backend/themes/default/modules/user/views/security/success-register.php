<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<h2>Пользователь успешно создан</h2>

<div>
    
    <?= Html::a('Вернуться на главную', Url::toRoute(['/core/index/index']), ['class' => 'btn btn-default']); ?>
    <?= Html::a('Создать еще одного', Url::toRoute(['/user/security/register-user']), ['class' => 'btn btn-primary']); ?>

</div>