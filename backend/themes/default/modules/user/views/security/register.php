<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<h2>Регистрация нового пользователя</h2>

<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'role')->dropDownList($model->getListRoles()); ?>

    <?= $form->field($model, 'email'); ?>

    <?= $form->field($model, 'surname'); ?>

    <?= $form->field($model, 'name'); ?>

    <?= $form->field($model, 'login'); ?>

    <?= $form->field($model, 'password_hash')->passwordInput(); ?>

    <?= Html::submitInput('Зарегистрировать', ['class' => 'btn btn-primary']); ?> 

    <?= Html::a('Отмена', Url::toRoute(['/core/index/index']), ['class' => 'btn btn-default']); ?>

<?php ActiveForm::end(); ?>