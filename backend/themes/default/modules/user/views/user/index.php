<?php
use yii\grid\GridView;
?>

<h2>Пользователи</h2>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $columns,
]); ?>