<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\test\models\Section;
use yii\helpers\Url;
use backend\modules\test\models\Part;
?>

<h2>Обновление пользователя</h2>

<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'surname'); ?>

    <?= $form->field($model, 'name'); ?>

    <?= $form->field($model, 'password'); ?>

    <?= $form->field($model, 'role')->dropDownList($model->getListRoles(), ['prompt' => 'Выберите роль']); ?>

    <?= $form->field($model, 'blocked')->dropDownList(['Активный', 'Заблокированный']); ?>

    <?= $form->field($model, 'sectionId')->dropDownList(ArrayHelper::map(Section::find()->all(), 'id', 'name'), ['prompt' => 'Выберите главу']); ?>

    <?= $form->field($model, 'partId')->dropDownList(($model->sectionId) ? ArrayHelper::map(Part::find()->where(['section_id' => $model->sectionId])->all(), 'id', 'title') : [], ['prompt' => 'Выберите раздел']); ?>

    <?= $form->field($model, 'status')->dropDownList($model->getListStatus(), ['prompt' => 'Выберите этап обучения']); ?>

    <?= Html::submitButton('Обновить', ['class' => 'btn btn-primary']); ?>

    <?= Html::a('Обнулить курс пользователя', Url::toRoute(['/user/user/reset', 'id' => Yii::$app->request->get('id')]), ['class' => 'btn btn-danger']); ?>

<?php ActiveForm::end(); ?>

<?php $this->registerJs('
    
    $("#usermodel-sectionid").change(function( e) {
        $.ajax({
            url: "' . Url::toRoute(['/user/user/get-list-parts']) .'",
            dataType: "JSON",
            data: {sectionId: $(this).val()},
            method: "POST",
        }).done(function(data) {
            if (data) {
                $("#usermodel-partid").replaceWith(data);
            }
        })
    })

'); ?>