<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'My Company',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/user/security/login']];
    } else {
        $menuItems[] = ['label' => 'Home', 'url' => ['/core/index/index']];
        $menuItems[] = ['label' => 'Структура сайта', 'items' => [
                                                      ['label' => 'Текстовые блоки', 'url' => ['/core/text-block/index']],
                                                  ],
        ];
        $menuItems[] = ['label' => 'Главы', 'url' => ['/test/section/index']];
        $menuItems[] = ['label' => 'Разделы', 'url' => ['/test/part/index']];
        $menuItems[] = ['label' => 'Раздел заданий', 'items' => [
                                                      ['label' => 'Задания', 'url' => ['/test/task/index']],
                                                      ['label' => 'Логи задания', 'url' => ['/test/task-log/index']],
                                                  ],
        ];
        $menuItems[] = ['label' => 'Рездел тестов', 'items' => [
                                                    ['label' => 'Тесты', 'url' => ['/test/test/index']],
                                                    ['label' => 'Вопросы', 'url' => ['/test/question/index']],
                                                    ['label' => 'Ответы', 'url' => ['/test/answer/index']],
                                                ],
        ];
        $menuItems[] = ['label' => 'Пользователи', 'items' => [
                                                    ['label' => 'Пользователи', 'url' => ['/user/user/index']],
                                                    ['label' => 'Создать нового пользователя', 'url' => ['/user/security/register-user']],
                                                ],
        ];
        $menuItems[] = '<li>'
            . Html::beginForm(['/user/security/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->login . ')',
                ['class' => 'btn btn-link']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
