<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\ServiceAsset;
use common\widgets\Alert;

$bundle = ServiceAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <?= Html::csrfMetaTags() ?>
        <meta charset="<?= Yii::$app->charset ?>">
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>

    <?php if (!Yii::$app->user->isGuest) : ?>

        <?= \frontend\modules\core\widgets\SupportWidget\SupportWidget::widget(); ?>

    <?php endif; ?>

    <div id="art-main">
        <div class="art-sheet clearfix">

            <?= $this->render('header'); ?>

            <?= $content; ?>

            <?= $this->render('footer', [
                'bundle' => $bundle,
            ]); ?>

        </div>
    </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>