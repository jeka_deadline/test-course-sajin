<?php
use yii\helpers\Url;
?>
<header class="art-header">
    <div class="art-shapes"></div>
    <nav class="art-nav">
        <ul class="art-hmenu">
            <li><a href="<?= Url::toRoute(['/core/index/index']); ?>">Главная</a></li>
            <li><a href="<?= Url::toRoute(['/user/security/logout']); ?>">Выход</a></li>
        </ul>
    </nav>
</header>
<hr color="#4d6580" align="center" width="100%" size="7px">
    <div style="overflow:hidden; margin:20px 0px">