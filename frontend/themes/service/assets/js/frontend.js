$(function() {
    $('.sub').click(function( e ) {
        var child = $(e.target).children('ul');
        if ($(e.target).find('.sub').length) {
            return false;
        }
        if (child) {
            if ($(child).hasClass('active')) {
                $(child).slideUp();
                $(child).removeClass('active');
                return;
            } else {
                $(child).slideDown();
                $(child).addClass('active');
                return;
            }
        }
    })
})