<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<h2>Авторизация пользователя</h2>

<br>

<div style="width: 300px;">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'login'); ?>
          
        <br>

        <?= $form->field($model, 'password')->passwordInput(); ?>

        <br>

        <?= Html::submitInput('Войти', ['class' => 'art-button']); ?>

    <?php ActiveForm::end(); ?>

</div>