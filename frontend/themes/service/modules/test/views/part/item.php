<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="row">
    <div class="col-lg-7">
        <h2><?= $model->title; ?></h2>
        <br>
        
        <div>
            
            <?= $model->text; ?>

        </div>

        <?= Html::beginForm(Url::toRoute(['/test/part/learn', 'id' => $model->id])); ?>
            
            <div class="checkbox">
                <label for="">
                    <br>
                    <?= Html::checkbox('agree'); ?> Я подтверждаю изучение
                </label>
            </div>

            <p class="error"><?= Yii::$app->request->get('error') ;?></p>

            <br>

            <?= Html::submitInput('Далее', ['class' => 'art-button']); ?>

        <?= Html::endForm(); ?>

    </div>
</div>