<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<h2>Список ответов (необходимо для успешного прохождения теста)</h2>
<br>

<div class="row">
    <div class="col-lg-7">
        
        <?php foreach ($questionsList as $question) : ?>

            <div>
                <b>Вопрос:</b><br> <?= $question->title; ?>
                <br><br>
                <b>Правильный ответ:</b><br> <?= $question->correct_answer; ?>
                <br><br><hr>
            </div>

        <?php endforeach; ?>

        <?= Html::beginForm(Url::toRoute(['/test/test/agree-repeat-part'])); ?>
            
            <div class="checkbox">
                <label for="">
                    <br>
                    <?= Html::checkbox('agree'); ?> Я подтверждаю изучение
                </label>
            </div>

            <p class="error"><?= Yii::$app->request->get('error') ;?></p>
            <br>

            <?= Html::submitInput('Далее', ['class' => 'art-button']); ?>

        <?= Html::endForm(); ?>

    </div>
</div>