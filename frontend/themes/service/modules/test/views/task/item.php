<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['/test/task/complete-task']),
]); ?>

    <div id="task-body">
      
        <?= $form->field($taskForm, 'type')->hiddenInput()->label(FALSE);; ?>

        <?= $form->field($taskForm, 'taskId')->hiddenInput()->label(FALSE);; ?>

        <h3><?= $task->title; ?></h3>

        <br>

        <p>
            
            <?= $task->text; ?>

        </p>

    </div>

    <br>

    <?= $form->field($taskForm, 'text')->textarea(['rows' => '10'])->label(FALSE); ?>

    <br>

    <?= Html::submitInput('Отправить задание', ['class' => 'art-button']); ?>

<?php ActiveForm::end(); ?>