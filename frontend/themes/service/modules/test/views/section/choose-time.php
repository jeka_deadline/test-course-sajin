<?php
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<?php if (!$delay) : ?>

    <div style="text-align:center">
      
        <?= $section->title_time; ?>

    </div>

<?php endif; ?>

<br>

<h5 style="text-align:center"><?= $text; ?></h5>
<br>

<?= Html::beginForm(Url::toRoute(['/test/section/set-time'])); ?>

    <?= Html::hiddenInput('section_id', $section->id); ?>

    <div class="well well-sm" style="background-color: #fff; width:245px; margin: 0 auto">

        <?= DatePicker::widget([
            'name' => 'time',
            'type' => DatePicker::TYPE_INLINE,
            'language' => 'ru',
            'pluginOptions' => [
                'format' => 'dd-mm-yyyy'
            ],
            'options' => [
            ]
        ]); ?>

    </div>
    <br>

    <?= Html::submitInput('Отправить', ['class' => 'art-button', 'style' => 'margin:0 auto !important; width:100px; display: block']); ?>

    <p class="error" style="text-align: center"><?= Yii::$app->request->get('error'); ?></p>

<?= Html::endForm(); ?>