<?php
use yii\helpers\Html;
?>

<div class="row" data-question="<?= $model->id; ?>">
    <div class="col-lg-12">
        <p>

            <?= $model->title; ?>
          
        </p>
        <br>
        <div>
            
            <?= Html::radioList('Answer[' . $model->id . ']', NULL, $model->getAnswersList(), [
                'item' => function ($index, $label, $name, $checked, $value) {
                  echo '<div  style="margin-top: 5px"><label>' . Html::radio($name, $checked, ['value' => $value]) . $label . '</label></div>';
              }]); ?>

        </div>
    </div>
    <div class="col-lg-12">
        <p class="error"></p>
    </div>
</div>
<hr>