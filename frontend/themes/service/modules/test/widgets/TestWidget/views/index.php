<?php
use yii\widgets\ListView;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<form method="POST" action="<?= Url::toRoute(['/test/test/answer']); ?>" id="form-test">

    <?= Html::hiddenInput('test_id', $test->id); ?>
    <input type="hidden" name="_csrf-frontend" value="<?=Yii::$app->request->getCsrfToken()?>">

    <?= ListView::widget([
        'dataProvider' => $listQuestions,
        'layout' => '{items}',
        'itemView' => '_form_question',
        'itemOptions' => [
            'tag' => FALSE,
        ]
    ]); ?>

    <?= Html::submitInput('Ответить', ['class' => 'art-button']); ?>

</form>