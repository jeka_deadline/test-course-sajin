<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Итог';

?>

<h2>Итог</h2>
<br>

<?= frontend\modules\core\widgets\TextBlockWidget\TextBlockWidget::widget(['uri' => 'finish']); ?>

<?= Html::beginForm(Url::toRoute(['/core/index/next-step'])); ?>

    <div class="checkbox">
        <label for="">
            <br>
            <?= Html::checkbox('next'); ?> Я прочитал итог
        </label>
    </div>
  
        <p class="error"> <?= Yii::$app->request->get('text'); ?></p><br>

    <?= Html::submitButton('К заключениею', ['class' => 'art-button']); ?>

<?= Html::endForm(); ?>