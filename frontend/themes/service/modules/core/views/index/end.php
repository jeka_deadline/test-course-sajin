<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Вы закончили обучение';

?>

<?= frontend\modules\core\widgets\TextBlockWidget\TextBlockWidget::widget(['uri' => 'end']); ?>