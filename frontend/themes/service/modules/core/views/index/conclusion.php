<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Заключение';

?>

<h2>Заключение</h2>
<br>

<?= frontend\modules\core\widgets\TextBlockWidget\TextBlockWidget::widget(['uri' => 'conclusion']); ?>

<?= Html::beginForm(Url::toRoute(['/core/index/next-step'])); ?>

    <div class="checkbox">
        <label for="">
            <br>
            <?= Html::checkbox('next'); ?> Я прочитал заключение
        </label>
    </div>
  
        <p class="error"> <?= Yii::$app->request->get('text'); ?></p>
        <br>
    <?= Html::submitButton('Закончить обучение', ['class' => 'art-button']); ?>

<?= Html::endForm(); ?>