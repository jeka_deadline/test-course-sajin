<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Введение';

?>

<h2>Введение</h2>
<br>

<?= frontend\modules\core\widgets\TextBlockWidget\TextBlockWidget::widget(['uri' => 'intro']); ?>

<?= Html::beginForm(Url::toRoute(['/core/index/next-step'])); ?>

    <div class="checkbox">
        <label for="">
            <br>
            <?= Html::checkbox('next'); ?> Я прочитал введение
        </label>
    </div>
  
        <p class="error"> <?= Yii::$app->request->get('text'); ?></p>

        <br>

    <?= Html::submitButton('Дальше', ['class' => 'art-button']); ?>

<?= Html::endForm(); ?>