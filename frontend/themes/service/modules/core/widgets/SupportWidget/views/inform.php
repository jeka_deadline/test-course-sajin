<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>

<?= $text; ?>

<br><br>

<?= Html::buttonInput('Попробовать снова', ['class' => 'art-button btn-repeat']); ?>
<?= Html::hiddenInput(NULL, Url::toRoute(['/core/index/get-support-form'])); ?>