<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['/core/index/support']), 'id' => 'support-form', 'enableAjaxValidation' => FALSE]); ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 10])->label(FALSE); ?>

    <br>

    <?= Html::submitInput('Задать вопрос', ['class' => 'art-button']); ?>

<?php ActiveForm::end(); ?>