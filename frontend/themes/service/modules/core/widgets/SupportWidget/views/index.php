<div class="panel">
    <a class="handle" href="#">Смотреть</a> <!-- Ссылка для пользователей с отключенным JavaScript -->
    <h3><span>Задайте вопрос нашему менеджеру</span></h3><br>
    <span id="content">

        <?= $this->render('_form', ['model' => $model]); ?>
      
    </span>
</div>