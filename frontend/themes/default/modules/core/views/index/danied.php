<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
Доступ к данной странице запрещен
<br>
<?= Html::a('Назад', Url::toRoute(['/core/index/index']), ['class' => 'btn btn-default']); ?>