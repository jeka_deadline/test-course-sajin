<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';

?>

<?= frontend\modules\core\widgets\TextBlockWidget\TextBlockWidget::widget(['uri' => 'intro']); ?>

<?= Html::beginForm(Url::toRoute(['/core/index/next-step'])); ?>

    <div class="checkbox">
        <label for="">
            <?= Html::checkbox('next'); ?> Я прочитал введение
        </label>
    </div>
  
        <p class="error"> <?= Yii::$app->request->get('text'); ?></p>

    <?= Html::submitButton('Дальше', ['class' => 'btn btn-primary']); ?>

<?= Html::endForm(); ?>