<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Заключение';

?>

<?= frontend\modules\core\widgets\TextBlockWidget\TextBlockWidget::widget(['uri' => 'conclusion']); ?>

<?= Html::beginForm(Url::toRoute(['/core/index/next-step'])); ?>

    <div class="checkbox">
        <label for="">
            <?= Html::checkbox('next'); ?> Я прочитал заключение
        </label>
    </div>
  
        <p class="error"> <?= Yii::$app->request->get('text'); ?></p>

    <?= Html::submitButton('Закончить обучение', ['class' => 'btn btn-primary']); ?>

<?= Html::endForm(); ?>