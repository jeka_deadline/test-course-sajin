<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<h2>Авторизация пользователя</h2>

<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'login'); ?>

    <?= $form->field($model, 'password')->passwordInput(); ?>

    <?= Html::submitInput('Войти', ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>