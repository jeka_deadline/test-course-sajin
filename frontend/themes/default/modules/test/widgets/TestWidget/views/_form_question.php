<?php
use yii\helpers\Html;
?>

<div class="row" data-question="<?= $model->id; ?>">
    <div class="col-lg-12">
        <p>

            <?= $model->title; ?>
          
        </p>
        <div>
            
            <?= Html::radioList('Answer[' . $model->id . ']', NULL, $model->getAnswersList()); ?>

        </div>
    </div>
    <div class="col-lg-12">
        <p class="error"></p>
    </div>
</div>