<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="row">
    <div class="col-lg-7">
        <p><?= $model->title; ?></p>
        
        <div>
            
            <?= $model->text; ?>

        </div>

        <?= Html::beginForm(Url::toRoute(['/test/part/learn', 'id' => $model->id])); ?>
            
            <div class="checkbox">
                <label for="">
                    <?= Html::checkbox('agree'); ?> Я подтверждаю изучение
                </label>
            </div>

            <p class="error"><?= Yii::$app->request->get('error') ;?></p>

            <?= Html::submitInput('Далее', ['class' => 'btn btn-primary']); ?>

        <?= Html::endForm(); ?>

    </div>
</div>