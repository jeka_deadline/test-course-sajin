<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<h2>Повторение раздела (необходимо для успешного прохождения теста)</h2>

<div class="row">
    <div class="col-lg-7">
        <p><?= $part->title; ?></p>
        
        <div>
            
            <?= $part->text; ?>

        </div>

        <?= Html::beginForm(Url::toRoute(['/test/test/agree-repeat-part'])); ?>
            
            <div class="checkbox">
                <label for="">
                    <?= Html::checkbox('agree'); ?> Я подтверждаю изучение
                </label>
            </div>

            <?= Html::hiddenInput('part_id', $part->id); ?>

            <p class="error"><?= Yii::$app->request->get('error') ;?></p>

            <?= Html::submitInput('Далее', ['class' => 'btn btn-primary']); ?>

        <?= Html::endForm(); ?>

    </div>
</div>