<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['/test/task/complete-task']),
]); ?>

    <div id="task-body">
      
        <?= $form->field($taskForm, 'type')->hiddenInput()->label(FALSE);; ?>

        <?= $form->field($taskForm, 'taskId')->hiddenInput()->label(FALSE);; ?>

        <p>

            <?= $task->title; ?>

        </p>

        <p>
            
            <?= $task->text; ?>

        </p>

    </div>

    <?= $form->field($taskForm, 'text')->textarea(['rows' => '10'])->label(FALSE); ?>

    <?= Html::submitInput('Отправить задание', ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>