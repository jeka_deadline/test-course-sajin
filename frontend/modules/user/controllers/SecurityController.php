<?php
namespace frontend\modules\user\controllers;

use Yii;
use frontend\modules\user\models\LoginForm;

class SecurityController extends \yii\web\Controller
{

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goHome();
        }

        $this->layout = '@app/views/layouts/main';

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}