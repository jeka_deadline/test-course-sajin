<?php
namespace frontend\modules\user;

use Yii;

class Module extends \yii\base\Module
{

    public $controllerNamespace = 'frontend\modules\user\controllers';

    public function init()
    {
        parent::init();
    }

}