<?php

namespace frontend\modules\user\models;

use Yii;
use common\models\user\LearnStatus as BaseLearnStatus;

class LearnStatus extends BaseLearnStatus
{

    public static function createNewLearn($userId)
    {
        $model          = new self();
        $model->user_id = $userId;
        $model->status  = 'start';
        $model->save();

        return $model;
    }

    public function generateVerifyMd5()
    {
        $block = '';
        $time = LearnTime::find()
                              ->where(['user_id' => Yii::$app->user->identity->getId()])
                              ->andWhere(['section_id' => $this->section_id])
                              ->one();

        if (!$time || $time->is_send_delay) {
            $block = 'block';
        }

        return md5('section_id' . $this->section_id . 'part_id' . $this->part_id . 'test_id' . $this->test_id . 'task_section_id' . $this->task_section_id . 'task_part_id' . $this->task_part_id . 'status' . $this->status . $block);
    }

    public function clearFields($attributes)
    {
        if (is_array($attributes)) {
            foreach ($attributes as $attribute) {
                $this->$attribute = NULL;
            }
        }
    }

    public function resetAll()
    {
        $this->clearFields(['section_id', 'part_id', 'task_section_id', 'task_part_id', 'test_id']);
        $this->status = 'start';
    }

    public function getTemplate($status)
    {
        switch ($status) {
            case 'finish':

                
                if ($this->status === 'finish') {
                    return 'finish';
                }
                if (empty($this->status) ||  $this->status === 'start') {
                    return 'danied';
                }
                return 'finish-learn';
                
                throw new \Exception("Error Processing Request", 1);
                
            case 'start';
                if ($this->status === 'start') {
                    return 'start';
                }
                return 'start-learn';
            case 'conclusion':
                if ($this->status === 'conclusion') {
                    return 'conclusion';
                }
                if (empty($this->status) || $this->status === 'start' || $this->status === 'finish') {
                    return 'danied';
                }
                return 'conclusion-learn';
            case 'end':
                if ($this->status === 'end') {
                    return 'end';
                }
                return 'danied';
        }
    }

}
