<?php
namespace frontend\modules\user\models;

use Yii;

class LoginForm extends \yii\base\Model
{

    public $login;
    public $password;

    public function rules()
    {
        return [
            [['login', 'password'], 'required', 'message' => 'Поле {attribute} не должно быть пустым'],
        ];
    }

    public function login()
    {
        $user = User::find()
                          ->where(['login' => $this->login])
                          ->andWhere(['blocked' => '0'])
                          ->one();

        if ($user && $user->validatePassword($this->password)) {
            
            if (!$user->first_enter) {
                $user->first_enter = time();
            }

            $user->last_enter = time();
            
            $user->save();

            Yii::$app->user->login($user);
            $this->sendMessageLogin($user);
            
            return TRUE;
        
        }

        return FALSE;
    }

    private function sendMessageLogin(User $user)
    {
        $subject = 'Вход пользователя на курс recru_tec';
        $text    = 'Пользователь:<br><b>Фамилия:</b> ' . $user->surname . ',<br><b>Имя:</b> ' . $user->name . ',<br><b>IP:</b> ' . Yii::$app->request->userIP;
        $text   .= '<br><br><hr color="#800000" align="left" width="95%"" size="7px"><br><br><a href="http://vlu.su/"" target="_blank"><img width="140" height="76" alt="" src="http://nofailhiring.ru/promotion/vlu.png"></a><br><br> С уважением, команда «Высшая Лига Управления» <br><br> Тел. +7 (965) 240 49 90<br><br> Тел. +7 926 700 82 71<br><br>Техподдержка: info@vlu.su'; 
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= 'From: course@vlu.su';

        mail(Yii::$app->params[ 'adminEmail' ], $subject, $text, $headers);
    }

    public function attributeLabels()
    {
        return [
            'login'     => 'Логин',
            'password'  => 'Пароль',
        ];
    }

}