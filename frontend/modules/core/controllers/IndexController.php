<?php
namespace frontend\modules\core\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\modules\user\models\LearnStatus;
use frontend\modules\test\models\Section;
use yii\helpers\Url;
use frontend\modules\user\models\LearnTime;
use frontend\modules\core\models\SupportForm;
use yii\web\Response;

class IndexController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index2', 'next-step'],
                'rules' => [
                    [
                        'actions' => ['index2', 'next-step', 'support', 'get-support-form'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionStatus($status)
    {
        $userId = Yii::$app->user->identity->getId();
        $model  = LearnStatus::find()
                                ->where(['user_id' => $userId])
                                ->one();

        return $this->render($model->getTemplate($status));
    }

    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/security/login']);
        }

        $userId = Yii::$app->user->identity->getId();
        $model  = LearnStatus::find()
                                ->where(['user_id' => $userId])
                                ->one();

        if (!$model) {
            $model = LearnStatus::createNewLearn($userId);
        }

        $learnTime = LearnTime::find()
                                  ->where(['user_id' => $userId])
                                  ->andWhere(['section_id' => $model->section_id])
                                  ->one();

        if ((!$learnTime || $learnTime->is_send_delay) && empty($model->status)) {
            $model->updateAttributes(['block_route' => '/test/section/choose-time']);
        }

        if ($model->block_route) {
            return $this->redirect([$model->block_route]);
        }

        if ($model->status) {
            if ($text = Yii::$app->request->get('text')) {

              return $this->redirect(Url::toRoute(['/core/index/status', 'status' => $model->status, 'text' => $text]));

            } else {

                return $this->redirect(Url::toRoute(['/core/index/status', 'status' => $model->status]));

            }
        }

        if ($model->task_part_id) {
            return $this->redirect(['/test/task/item', 'id' => $model->task_part_id]);
        }

        if ($model->task_section_id) {
            return $this->redirect(['/test/task/item', 'id' => $model->task_section_id]);
        }

        if ($model->test_id) {
            return $this->redirect(['/test/test/item', 'id' => $model->test_id]);
        }

        if ($model->part_id) {
            return $this->redirect(['/test/part/item', 'id' => $model->part_id]);
        }

        if ($model->section_id) {
            return $this->redirect(['/test/section/item', 'id' => $model->section_id]);
        }

        $model->resetAll();
        $model->save();
        $this->refresh();
        
    }

    public function actionSupport()
    {
        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\NotFoundHttpException();
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        $supportForm                = new SupportForm();

        if ($supportForm->load(Yii::$app->request->post()) && $supportForm->validate()) {

            $text = ($supportForm->send()) ? 'Ваш вопрос успешно задан, скоро с Вами свяжется наш менеджер' : 'Ошибка почтового сервера';

        } else {

            $text = 'Произошла ошибка, попробуйте позже';

        }

        return ['content' => $this->renderAjax('@frontend/themes/service/modules/core/widgets/SupportWidget/views/inform', ['text' => $text])];
    }

    public function actionGetSupportForm()
    {
        $supportForm = new SupportForm();

        return $this->renderAjax('@frontend/themes/service/modules/core/widgets/SupportWidget/views/_form', ['model' => $supportForm]);
    }

    public function actionNextStep()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $agree  = Yii::$app->request->post('next');
        $text   = '';
        $model  = LearnStatus::find()
                                ->where(['user_id' => Yii::$app->user->identity->getId()])
                                ->one();

        if ($agree) {

            if ($model && $model->status == 'conclusion') {
                $model->status = 'end';
                $model->save();
            }

            if ($model && $model->status == 'finish') {
                $model->status = 'conclusion';
                $model->save();
            }

            if ($model && $model->status == 'start') {
                $section = Section::find()
                                        ->where(['active' => '1'])
                                        ->orderBy(['display_order' => SORT_ASC])
                                        ->one();

                if ($section) {
                    $model->section_id = $section->id;
                    $model->status = NULL;
                    $model->save();
                }
            }

        } else {
            $text = 'Поставьте галочку';
        }

        return $this->redirect(['/core/index/index', 'text' => $text]);

    }

}
