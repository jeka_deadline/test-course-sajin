<?php
namespace frontend\modules\core\models;

use Yii;
use yii\base\Model;

class SupportForm extends Model
{

    public $text;

    public function rules()
    {
        return [
            [['text'], 'required', 'message' => 'Текст вопроса является обязательным'],
            [['text'], 'string'],
        ];
    }

    public function send()
    {
        $user = Yii::$app->user->identity;

        $subject = 'Обратная связь (вопрос пользователя) курс recru_tec';
        $text    = 'Пользователь:<br><b>Фамилия:</b> ' . $user->surname . '<br><b>Имя:</b> ' . $user->name . '<br><b>Email:</b> ' . $user->email . '<br>Задал вопрос, текс вопроса:<br>' . $this->text;
        $text   .= '<br><br><hr color="#800000" align="left" width="95%"" size="7px"><br><br><a href="http://vlu.su/"" target="_blank"><img width="140" height="76" alt="" src="http://nofailhiring.ru/promotion/vlu.png"></a><br><br> С уважением, команда «Высшая Лига Управления» <br><br> Тел. +7 (965) 240 49 90<br><br> Тел. +7 926 700 82 71<br><br>Техподдержка: info@vlu.su'; 
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= 'From: course@vlu.su';

        return mail(Yii::$app->params[ 'adminEmail' ], $subject, $text, $headers);
    }

}
?>