<?php
namespace frontend\modules\core\widgets\SupportWidget;

use yii\web\AssetBundle;

class SupportAsset extends AssetBundle
{

    public $js = [
        'js/site.js',
        'js/ajax-support.js',
    ];

    public $css = [
        'css/style.css',
    ];

    public $depends = [
        // we will use jQuery
        'yii\web\JqueryAsset',
    ];

    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];

    public function init()
    {   
        $this->sourcePath = __DIR__ . "/assets";
        parent::init();
    }

}