$(function(){

    $(document).on('submit', '#support-form', function(e) {
        var submitButton  = $(this).find('input[type=submit]');
        var self          = $(this);
        e.preventDefault();
        if ($(this).find('.has-error').length > 0) {
            return false;
        }
        $(submitButton).val('Отправка вопроса...');
        $.ajax({
            url: $(this).attr('action'),
            data: $(this).serialize(),
            method: 'POST',
            dataType: 'JSON'
        }).done(function(data) {
            if (data) {
                $(submitButton).val('Задать вопрос');
                $('#content').empty().append(data.content);
            }
        })
    });

    $('#content').on('click', '.btn-repeat', function( e ) {
        $.ajax({
            url: $(this).closest('#content').find('input[type=hidden]').val(),
        }).done(function(data) {
            if (data) {
                $('#content').empty().append(data);
            }
        });
    });

});