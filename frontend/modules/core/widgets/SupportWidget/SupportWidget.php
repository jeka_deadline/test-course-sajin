<?php
namespace frontend\modules\core\widgets\SupportWidget;

use yii\base\Widget;
use frontend\modules\core\models\SupportForm;

class SupportWidget extends Widget
{

    public function init()
    {
        $view = $this->getView();
        $bundle = SupportAsset::register($this->getView());
        
        $view->registerJs('$(function(){
            $(".panel").tabSlideOut({                       //Класс панели
                tabHandle: ".handle",                       //Класс кнопки
                pathToTabImage: "' . $bundle->baseUrl . '/img/imgbutton.jpg",            //Путь к изображению кнопки
                imageHeight: "122px",                       //Высота кнопки
                imageWidth: "40px",                         //Ширина кнопки
                tabLocation: "right",                       //Расположение панели top - выдвигается сверху, right - выдвигается справа, bottom - выдвигается снизу, left - выдвигается слева
                speed: 300,                                 //Скорость анимации
                action: "click",                            //Метод показа click - выдвигается по клику на кнопку, hover - выдвигается при наведении курсора
                topPos: "200px",                            //Отступ сверху
                fixedPosition: true                        //Позиционирование блока false - position: absolute, true - position: fixed
            });
        });'
        );
    }

    public function run()
    {
        $model = new SupportForm();

        return $this->render('index', [
            'model' => $model,
        ]);
    }

}
?>