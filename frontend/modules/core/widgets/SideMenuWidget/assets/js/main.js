$(function(){
    var headers = ["H1","H2","H3","H4","H5","H6"];

    if ($('.active').closest('div.section').length) {
        var block = $('.active').closest('div');
        while (block) {
            $(block).addClass('opened-for-codepen');
            var div = $(block).parents('div');

            block =  ($(div).closest('.accordion').length) ? div : null;
        }
    }

    $(".accordion").click(function(e) {
      if (e.target.tagName === 'IMG') {
          var target = $(e.target).parent();
          var name = $(target)[0].tagName;
          name.replace(/['"]/, '', name);
      } else {
          var target = e.target;
          var name = target.nodeName.toUpperCase();
      }
      
      if($.inArray(name,headers) > -1) {
        var subItem = $(target).next();
        
        //slideUp all elements (except target) at current depth or greater
        var depth = $(subItem).parents().length;
        var allAtDepth = $(".accordion p, .accordion div").filter(function() {
          if($(this).parents().length >= depth && this !== subItem.get(0)) {
            return true; 
          }
        });
        $(allAtDepth).slideUp("fast");
        
        //slideToggle target content and adjust bottom border if necessary
        subItem.slideToggle("fast",function() {
        });
        $(target).css({"border-bottom-right-radius":"0", "border-bottom-left-radius":"0"});
      }
    });

});