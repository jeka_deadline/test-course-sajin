<?php
namespace frontend\modules\core\widgets\SideMenuWidget;

use Yii;
use yii\base\Widget;
use frontend\modules\test\models\Section;
use yii\helpers\Url;
use frontend\modules\user\models\LearnStatus;

class SideMenuWidget extends Widget
{

    private $_bundle;

    public function init()
    {
        $this->_bundle = SideMenuAsset::register($this->getView());
    }

    public function run()
    {

        $status = LearnStatus::find()
                                  ->where(['user_id' => Yii::$app->user->identity->getId()])
                                  ->one();

        $menus = [];
        $sections = Section::find()
                                ->where(['active' => '1'])
                                ->orderBy(['display_order' => SORT_ASC])
                                ->all();

        $menus[] = [
            'name' => 'Введение',
            'children' => FALSE,
            'page' => [
                'url'   => Url::toRoute(['/core/index/status', 'status' => 'start']),
                'class' => ($status->status == 'start') ? 'active' : 'learn',
            ],
            'parts' => [],
            'tasks' => [],
            'test'  => '',
        ];

        $i = count($menus);

        foreach ($sections as $section) {

            $menus[ $i ] = [
                'name'  => $section->name,
                'children' => TRUE,
                'page'  => [
                    'url'   => Url::toRoute(['/test/section/item', 'id' => $section->id]),
                    'class' => $section->getStatusForSidebar($status),
                ], 
                'parts' => [],
                'tasks' => [],
                'test'  => '',
            ];

            if ($parts = $section->getParts()->where(['active' => '1'])->orderBy(['display_order' => SORT_ASC])->all()) {
                
                foreach ($parts as $part) {

                    $menus[ $i ][ 'parts' ][ $part->id ] = [
                        'name' => $part->title,
                        'page' => [
                            'url'   => Url::toRoute(['/test/part/item', 'id' => $part->id]),
                            'class' => $part->getStatusForSidebar($status),
                        ],
                        'tasks' => [],
                    ];

                    if ($tasks = $part->getTasks()->where(['active' => '1'])->orderBy(['display_order' => SORT_ASC])->all()) {
                        foreach ($tasks as $task) {

                            $menus[ $i ][ 'parts' ][ $part->id ][ 'tasks' ][ $task->id ] = [
                                'name' => $task->title,
                                'page' => [
                                    'url'   => Url::toRoute(['/test/task/item', 'id' => $task->id]),
                                    'class' => $task->getStatusForSidebar($status),
                                ],
                            ];

                        }
                    }

                }
            }
            if ($tasks = $section->getTasks()->where(['active' => '1'])->all()) {
                foreach ($tasks as $task) {

                    $menus[ $i ]['tasks'][ $task->id ] = [
                        'name' => $task->title,
                        'page' => [
                            'url'   => Url::toRoute(['/test/task/item', 'id' => $task->id]),
                            'class' => $task->getStatusForSidebar($status),
                        ],
                    ];

                }
            }

            if ($test = $section->test) {

                $menus[ $i ]['test'] = [
                    'name' => $test->name,
                    'page' => [
                        'url'   => Url::toRoute(['/test/test/item', 'id' => $test->id]),
                        'class' => $test->getStatusForSidebar($status),
                    ],
                ];

            }

            $i++;
        }

        $classFinish = '';

        if ($status->status === 'finish') {
            $classFinish = 'active';
        } else {
            $classFinish = ($status->status && $status->status !== 'start') ? 'learn' : 'block';
        }

        $menus[] = [
            'name' => 'Итог',
            'children' => FALSE,
            'page' => [
                'url' => Url::toRoute(['/core/index/status', 'status' => 'finish']),
                'class' => $classFinish,
            ],
            'parts' => [],
            'tasks' => [],
            'test'  => '',
        ];

        $classConclusion = '';

        if ($status->status === 'conclusion') {
            $classConclusion = 'active';
        } else {
            $classConclusion = ($status->status === 'end') ? 'learn' : 'block';
        }

        $menus[] = [
            'name' => 'Заключение',
            'children' => FALSE,
            'page' => [
                'url' => Url::toRoute(['/core/index/status', 'status' => 'conclusion']),
                'class' => $classConclusion,
            ],
            'parts' => [],
            'tasks' => [],
            'test'  => '',
        ];
        
        return $this->render('index', [
            'menus'   => $menus,
            'bundle'  => $this->_bundle,
        ]);
    }

}