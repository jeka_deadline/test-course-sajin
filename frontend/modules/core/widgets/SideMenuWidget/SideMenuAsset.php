<?php
namespace frontend\modules\core\widgets\SideMenuWidget;

use yii\web\AssetBundle;

class SideMenuAsset extends AssetBundle
{

    public $js = [
        'js/main.js',
    ];

    public $css = [
        'css/style.css',
    ];

    public $depends = [
        // we will use jQuery
        'yii\web\JqueryAsset'
    ];

    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];

    public function init()
    {   
        $this->sourcePath = __DIR__ . "/assets";
        parent::init();
    }

}