<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<aside class="accordion">

    <?php foreach ($menus as $itemMemu) : ?>
        <?php if ($itemMemu[ 'children' ]) : ?>

            <h1 class="<?= $itemMemu[ 'page' ][ 'class' ]; ?>"><img src="<?= $bundle->baseUrl . '/img/add-icon--icon-search-engine-1.png'; ?>"><?= $itemMemu[ 'name' ]; ?></h1>

            <div class="section">

                <?php if ($itemMemu[ 'parts' ]) : ?>

                    <h2><img src="<?= $bundle->baseUrl . '/img/add-icon--icon-search-engine-1.png'; ?>">Разделы</h2>
                    <div>
                      
                        <?php foreach ($itemMemu[ 'parts' ] as $part) : ?>

                            <?= Html::a($part[ 'name' ], $part[ 'page' ][ 'url' ], ['class' => $part[ 'page' ][ 'class' ]]); ?>

                            <?php if ($part[ 'tasks' ]) : ?>

                                  <h3><img src="<?= $bundle->baseUrl . '/img/add-icon--icon-search-engine-1.png'; ?>"> Задания к разделу</h3>
                                  
                                  <div>

                                      <?php foreach ($part[ 'tasks' ] as $task) : ?>

                                          <?= Html::a($task[ 'name' ], $task[ 'page' ][ 'url' ], ['class' => $task[ 'page' ][ 'class' ]]); ?>

                                      <?php endforeach; ?>

                                  </div>

                            <?php endif; ?>
                        <?php endforeach; ?>

                    </div>

                <?php endif; ?>
                <?php if ($itemMemu[ 'test' ]) : ?>

                    <h2><img src="<?= $bundle->baseUrl . '/img/add-icon--icon-search-engine-1.png'; ?>"> Тестирование</h2>
                    <div>
                            
                        <?= Html::a('Тест по главе', $itemMemu[ 'test' ][ 'page' ][ 'url' ], ['class' => $itemMemu[ 'test' ][ 'page' ][ 'class' ]]); ?>

                    </div>

                <?php endif; ?>
                <?php if ($itemMemu[ 'tasks' ]) : ?>
                    
                        <h2><img src="<?= $bundle->baseUrl . '/img/add-icon--icon-search-engine-1.png'; ?>">Задания к главе</h2>
                        <div>

                            <?php foreach ($itemMemu[ 'tasks' ] as $task) : ?>
                                    
                                <?= Html::a($task[ 'name' ], $task[ 'page' ][ 'url' ], ['class' => $task[ 'page' ][ 'class' ]]); ?>

                            <?php endforeach; ?>

                        </div>

                <?php endif; ?>

            </div>

        <?php else : ?>

          <?= Html::a($itemMemu[ 'name' ], $itemMemu[ 'page' ][ 'url' ], ['class' => $itemMemu[ 'page' ][ 'class' ]]); ?>

        <?php endif; ?>
    <?php endforeach; ?>

</aside>