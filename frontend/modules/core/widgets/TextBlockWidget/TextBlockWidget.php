<?php
namespace frontend\modules\core\widgets\TextBlockWidget;

use Yii;
use yii\base\Widget;
use common\models\core\TextBlock;

class TextBlockWidget extends Widget
{

    public $uri;

    public function run()
    {
        $block = TextBlock::find()
                            ->where(['uri' => $this->uri])
                            ->andWhere(['active' => '1'])
                            ->one();

        if ($block) {
            echo $block->content;
        }
    }

}
?>