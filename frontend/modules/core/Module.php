<?php
namespace frontend\modules\core;

use yii\base\Module as BaseModule;

class Module extends BaseModule
{

    public $controllerNamespace = 'frontend\modules\core\controllers';

    public function init()
    {
        parent::init();
    }

}