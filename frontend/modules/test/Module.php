<?php
namespace frontend\modules\test;

use Yii;

class Module extends \yii\base\Module
{

    public $controllerNamespace = 'frontend\modules\test\controllers';

    public function init()
    {
        parent::init();
    }

}