$(function() {

    $(document).on('submit', '#form-test', function(e) {
        e.preventDefault();
        $('p.error').empty();
        $.ajax({
            url: $(this).attr('action'),
            data: $(this).serialize(),
            method: $(this).attr('method'),
            dataType: 'JSON',
        }).done(function(data) {
            if (data) {
                if (data.status === 'error') {
                    $.each(data.dataErrors, function(index, $object){
                        var row = $('[data-question="' + $object.question_id + '"]');
                        $(row).find('p.error').text($object.text);
                    });

                    if (data.repeat) {
                        $('#form-test').find('[type=submit]').remove();
                        $('#form-test').append(data.text);
                    }
                } else {
                    if (data.status === 'success') {
                        $('#form-test').replaceWith(data.text);
                    }
                }
            }
        })
    });

});