<?php
namespace frontend\modules\test\widgets\TestWidget;

use Yii;
use yii\base\Widget;
use frontend\modules\test\models\Test;
use yii\data\ArrayDataProvider;

class TestWidget extends Widget
{

    public $testId;

    public function init()
    {
        TestAsset::register($this->getView());
    }

    public function run()
    {
        $test     = Test::findOne($this->testId);
        $session  = Yii::$app->session;

        if (!$test) {
            return FALSE;
        }

        $query = $test->getQuestions()->where(['active' => '1']);

        if ($session->has('wrongQuestionsId')) {
            $query->andWhere(['in', 'id', $session->get('wrongQuestionsId')]);
        }

        $listQuestions = new ArrayDataProvider([
            'allModels' => $query->all(),
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

        return $this->render('index', [
            'test'          => $test,
            'listQuestions' => $listQuestions,
        ]);
    }

}
?>