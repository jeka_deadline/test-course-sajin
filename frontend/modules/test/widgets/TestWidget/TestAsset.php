<?php
namespace frontend\modules\test\widgets\TestWidget;

use yii\web\AssetBundle;

class TestAsset extends AssetBundle
{

    public $js = [
        'js/test.js',
    ];

    public $css = [
    ];

    public $depends = [
        // we will use jQuery
        'yii\web\JqueryAsset'
    ];

    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];

    public function init()
    {   
        $this->sourcePath = __DIR__ . "/assets";
        parent::init();
    }

}