<?php
namespace frontend\modules\test\controllers;

use Yii;
use yii\web\Controller;
use frontend\modules\test\models\Task;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use frontend\modules\user\models\LearnStatus;
use frontend\modules\test\models\TaskForm;

class TaskController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['item', 'complete-task'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionItem($id)
    {
        $userId = Yii::$app->user->identity->getId();

        $model = Task::find()
                          ->where(['id' => $id])
                          ->andWhere(['active' => '1'])
                          ->one();

        if (!$model) {
            throw new NotFoundHttpException('Section not found');
        }

        $modelStatus  = LearnStatus::find()
                                ->where(['user_id' => $userId])
                                ->one();

        if ($modelStatus->block_route) {
            return $this->redirect([$modelStatus->block_route]);
        }

        if ($modelStatus->generateVerifyMd5() !== $model->generateVerifyMd5()) {

            $this->layout = '@app/views/layouts/main';
            return $this->render('@frontend/themes/service/modules/core/views/index/danied');
        }

        $type = 'part';

        if ($model->section) {
            $type = 'section';
        }

        $taskForm         = new TaskForm();
        $taskForm->type   = $type;
        $taskForm->taskId = $model->id;

        return $this->render('item', [
            'task'      => $model,
            'taskForm'  => $taskForm,
        ]);
    }

    public function actionCompleteTask()
    {
        $taskForm = new TaskForm();

        if ($taskForm->load(Yii::$app->request->post()) && $taskForm->validate()) {
            $model = Task::find()
                              ->where(['id' => $taskForm->taskId])
                              ->andWhere(['active' => '1'])
                              ->one();

            if (!$model) {
                throw new NotFoundHttpException('Task not found');
            }

            $taskForm->sendMessage();

            $response = $this->nextStep($taskForm, $model);

            if ($response) {

                $this->layout = '@app/views/layouts/main';
                return $this->render('@frontend/themes/service/modules/core/views/index/inform', [
                    'text' => $response,
                ]);
            }

            //throw new \Exception($response);
            

            return $this->redirect(['/core/index/index']);
        }

    }

    private function nextStep(TaskForm $taskForm, Task $task)
    {
        $userId = Yii::$app->user->identity->getId();

        $modelStatus  = LearnStatus::find()
                                ->where(['user_id' => $userId])
                                ->one();

        if ($modelStatus->block_route) {
            return $this->redirect([$modelStatus->block_route]);
        }

        if ($taskForm->type === 'part') {
            if ($nextTask = $task->getNextPartTask()) {
                
                $modelStatus->task_part_id    = $nextTask;

                $modelStatus->clearFields(['test_id', 'task_section_id', 'status']);
                $modelStatus->save();

                return NULL;
            }

            if ($nextPart = $task->getNextPart()) {
                
                $modelStatus->part_id  = $nextPart;

                $modelStatus->clearFields(['test_id', 'task_section_id', 'task_part_id', 'status']);
                $modelStatus->save();

                return 'Вы выполнили все задания по данному разделу, для перехода на следующий раздел нажмите кнопку ниже';
            }

            if ($test = $task->getTest()) {              

                $modelStatus->test_id = $test;

                $modelStatus->clearFields(['task_part_id', 'status', 'task_section_id', 'part_id']);
                $modelStatus->save();

                return 'Вы выполнили все задания по данному разделу, для перехода на тест нажмите кнопку ниже';
            }

            if ($nextTaskSection = $task->getSectionTaks()) {
                $modelStatus->task_section_id = $nextTaskSection;

                $modelStatus->clearFields(['task_part_id', 'status', 'test_id', 'part_id']);
                $modelStatus->save();

                return 'Вы выполнили все задания по данному разделу, для перехода к заданиям по всей главе нажмите кнопку ниже';
            }

            if ($nextSection = $task->getNextSection()) {

                $modelStatus->section_id = $nextSection;

                $modelStatus->clearFields(['test_id', 'task_section_id', 'task_part_id', 'status', 'part_id']);
                $modelStatus->save();

                return 'Вы выполнили все задания по данному разделу, для перехода на следующую главу нажмите кнопку ниже';
            }

            $modelStatus->status = 'finish';

            $modelStatus->clearFields(['test_id', 'task_section_id', 'task_part_id', 'part_id', 'section_id']);
            $modelStatus->save();

            return 'Вы выполнили все задания по последней главе, для перехода к итогу нажмите кнопку ниже';
        }

        if ($taskForm->type === 'section') {
            if ($nextTask = $task->getNextSectionTask()) {

                $modelStatus->task_section_id    = $nextTask;

                $modelStatus->clearFields(['test_id', 'task_part_id', 'status']);
                $modelStatus->save();

                return NULL;
            }
            
            /*if ($test = $task->getTest()) {
                $modelStatus->test_id = $test;
                
                $modelStatus->clearFields(['part_id', 'task_part_id', 'task_section_id', 'status']);
                $modelStatus->save();

                return 'Вы выполнили все задания по данной главе, для перехода к следующему пункту нажмите кнопку ниже';
            }*/

            if ($nextSection = $task->getNextSection()) {
                $modelStatus->section_id = $nextSection;

                $modelStatus->clearFields(['test_id', 'task_section_id', 'task_part_id', 'status', 'part_id']);
                $modelStatus->save();

                return 'Вы выполнили все задания по данной главе, для перехода на следуующую главу нажмите кнопку ниже';
            }

            $modelStatus->status = 'finish';

            $modelStatus->clearFields(['test_id', 'task_section_id', 'task_part_id', 'part_id', 'section_id']);
            $modelStatus->save();

            return 'Вы выполнили все задания по последней главе, для перехода к итогу нажмите кнопку ниже';
        }
    }

}