<?php
namespace frontend\modules\test\controllers;

use Yii;
use yii\web\Controller;
use frontend\modules\test\models\Part;
use yii\web\NotFoundHttpException;
use frontend\modules\user\models\LearnStatus;
use yii\filters\AccessControl;
use frontend\modules\test\models\Section;

class PartController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['item', 'learn'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionItem($id)
    {
        $userId = Yii::$app->user->identity->getId();

        $model = Part::find()
                          ->where(['id' => $id])
                          ->andWhere(['active' => '1'])
                          ->one();

        if (!$model) {
            throw new NotFoundHttpException('Part not found');
        }

        $modelStatus  = LearnStatus::find()
                                      ->where(['user_id' => $userId])
                                      ->one();

        if ($modelStatus->block_route) {
            return $this->redirect([$modelStatus->block_route]);
        }

        if (!$modelStatus) {
            return $this->redirect(['/core/index/index']);
        }

        $sectionPart = $model->section;

        $currentSection = Section::findOne($modelStatus->section_id);
        $currentPart = Part::findOne($modelStatus->part_id);

        if ($modelStatus->status && $modelStatus->status !== 'start') {
            $learn = TRUE;
        } else {
            if ($currentPart) {
                $learn = (($sectionPart->display_order < $currentSection->display_order || ($sectionPart->id === $currentSection->id && $model->display_order < $currentPart->display_order))) ? TRUE : FALSE;

            } else {
                
                $currentPart = $currentSection->getParts()->where(['active' => '1'])->orderBy(['display_order' => SORT_DESC])->one();
                $learn = (($sectionPart->display_order < $currentSection->display_order || ($sectionPart->id === $currentSection->id && $model->display_order <= $currentPart->display_order))) ? TRUE : FALSE;

            }
        }

        if (!$learn) {

            if ($modelStatus->generateVerifyMd5() !== $model->generateVerifyMd5()) {
                return $this->render('@frontend/themes/service/modules/core/views/index/danied');
            }

        }

        $this->view->title = 'Recru-tec | Раздел - ' . $model->title;

        $template = (!$learn) ? 'item' : 'item-learn';

        return $this->render($template, [
            'model' => $model,
        ]);
    }

    public function actionLearn($id)
    {
        $userId = Yii::$app->user->identity->getId();
        
        $model = Part::find()
                          ->where(['id' => $id])
                          ->andWhere(['active' => '1'])
                          ->one();

        if (!$model) {
            throw new NotFoundHttpException('Part not found');
        }

        $modelStatus  = LearnStatus::find()
                                      ->where(['user_id' => $userId])
                                      ->one();

        if ($modelStatus->block_route) {
            return $this->redirect([$modelStatus->block_route]);
        }

        if ($modelStatus->generateVerifyMd5() !== $model->generateVerifyMd5()) {

            $this->layout = '@app/views/layouts/main';
            return $this->render('@frontend/themes/service/modules/core/views/index/danied');
        }

        $agree = Yii::$app->request->post('agree');

        if (!$agree) {
            return $this->redirect(['/test/part/item', 'id' => $id, 'error' => 'Вы не подтвердили изучение раздела']);
        }

        if ($listTasks = $model->getListTasks()) {
            
            $modelStatus->task_part_id = $listTasks[0]->id;
            
            $modelStatus->clearFields(['test_id', 'task_section_id', 'status']);
            $modelStatus->save();

            $this->layout = '@app/views/layouts/main';
            return $this->render('@frontend/themes/service/modules/core/views/index/inform', ['text' => 'Вы изучили данный раздел, сейчас Вы должны выполнить некоторые задания']);

            //return $this->redirect(['/core/index/index']);
        }

        if ($model->isLastPart()) {

            if ($testId = $model->getTest()) {
                $modelStatus->test_id = $testId;

                $modelStatus->clearFields(['task_section_id', 'task_part_id', 'status', 'part_id']);
                $modelStatus->save();

                $this->layout = '@app/views/layouts/main';
                return $this->render('@frontend/themes/service/modules/core/views/index/inform', ['text' => 'Вы изучили все разделы данной главы, сейчас Вы должны пройти тестирование по данной главе']);
            }

            if ($listTasks = $model->getListTasksForSection()) {
                $modelStatus->task_section_id = $listTasks[0]->id;
            
                $modelStatus->clearFields(['test_id', 'task_part_id', 'status', 'part_id']);
                $modelStatus->save();

                $this->layout = '@app/views/layouts/main';
                return $this->render('@frontend/themes/service/modules/core/views/index/inform', ['text' => 'Вы изучили все разделы данной главы, сейчас Вы должны будете выполнить задания по данной главе']);
                
            }

        }

        $nextPart = Part::find()
                            ->where(['active' => '1'])
                            ->andWhere(['>', 'display_order', $model->display_order])
                            ->one();

        if ($nextPart) {
            $modelStatus->part_id = $nextPart->id;
            $modelStatus->save();
            return $this->redirect(['/test/part/item', 'id' => $nextPart->id]);
        } else {
            return $this->redirect(['/test/section/succes-learn', 'id' => $model->section->id]);
        }
    }

}
?>