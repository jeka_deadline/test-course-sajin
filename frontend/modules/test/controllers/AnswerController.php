<?php
namespace frontend\modules\test\controllers;

use Yii;
use yii\web\Controller;
use frontend\modules\test\models\Test;
use frontend\modules\test\models\LogTest;
use yii\helpers\ArrayHelper;
use yii\web\Response;

class AnswerController extends Controller
{

    public function actionAnswer()
    {

        Yii::$app->response->format = Response::FORMAT_JSON;

        $response = ['status' => 'error', 'dataErrors' => []];
        $testId   = Yii::$app->request->post('test_id');

        $test     = Test::find()
                            ->where(['id' => $testId])
                            ->andWhere(['active' => '1'])
                            ->one();

        if (!$test) {
            return FALSE;
        }
        $logTest        = LogTest::find()
                                      ->where(['user_id' => '1'])
                                      ->andWhere(['test_id' => $testId])
                                      ->one();

        if (!$logTest) {
            $logTest                  = new LogTest();
            $logTest->user_id         = 1;
            $logTest->test_id         = $testId;
            $logTest->count_attempts  = 0;
        }

        $questionsList  = ArrayHelper::map($test->getQuestions()->where(['active' => '1'])->all(), 'id', function($model){return $model;});

        $postAnswer = Yii::$app->request->post('Answer');

        if (!$postAnswer) {
            foreach ($questionsList as $question) {
                $response[ 'dataErrors' ][] = ['question_id' => $question->id, 'text' => 'Вы не дали ответ на этот вопрос'];
            }

            $logTest->count_attempts++;
            $logTest->status = 'wrong';
            $logTest->save();

            return $response;
        }

        foreach ($questionsList as $question) {
            $answer = (isset($postAnswer[ $question->id ])) ? $postAnswer[ $question->id ] : NULL;

            if (!$answer) {
                $response[ 'dataErrors' ][] = ['question_id' => $question->id, 'text' => 'Вы не дали ответ на этот вопрос']; 
                continue; 
            }
            if (!$question->isCorrectAnswer($postAnswer[ $question->id ])) {
                $response[ 'dataErrors' ][] = ['question_id' => $question->id, 'text' => 'Вы неправильно ответили на этот вопрос'];
                continue; 
            }
        }

        if (empty($response[ 'dataErrors' ])) {

            $response[ 'status' ] = 'success';
            $logTest->count_attempts++;
            $logTest->status = 'ok';

        } else {

            $logTest->count_attempts++;
            $logTest->status = 'wrong';
            
        }

        $logTest->save();

        return $response;
    }

}
?>