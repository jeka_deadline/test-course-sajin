<?php
namespace frontend\modules\test\controllers;

use Yii;
use frontend\modules\test\models\Section;
use yii\web\Controller;
use frontend\modules\user\models\LearnStatus;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use frontend\modules\user\models\LearnTime;

class SectionController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['item', 'succes-learn', 'choose-time', 'set-time'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionSuccesLearn($id)
    {
        $this->layout = '@app/views/layouts/main';
        $userId = Yii::$app->user->identity->getId();
        
        $model = Section::find()
                            ->where(['id' => $id])
                            ->andWhere(['active' => '1'])
                            ->one();

        if (!$model) {
            throw new NotFoundHttpException('Section not found');
        }

        $modelStatus  = LearnStatus::find()
                                      ->where(['user_id' => $userId])
                                      ->one();

        if ($modelStatus->block_route) {
            return $this->redirect([$modelStatus->block_route]);
        }

        if (!$modelStatus) {
            return $this->redirect(['/core/index/index']);
        }

        if ($modelStatus->generateVerifyMd5() !== md5('section_id' . $model->id . 'part_id' . $model->getLastPart() . 'test_id' . NULL . 'task_section_id' . NULL . 'task_part_id' . NULL . 'status' . NULL)) {
            
            return $this->render('@frontend/themes/service/modules/core/views/index/danied');
        }

        $nextSection = Section::find()
                                ->where(['active' => '1'])
                                ->andWhere(['>', 'display_order', $model->display_order])
                                ->one();

        if ($nextSection) {
            $modelStatus->clearFields(['part_id']);
            $modelStatus->section_id  = $nextSection->id;
            $text                     = 'Вы изучили данную главу. Для того чтобы двигаться дальше нажмите кнопку ниже';
            
            
        } else {

            $modelStatus->status  = 'finish';
            $text                 = 'Вы изучили все главы, поздравляем Вас. Для перехода к итогу нажмите кнопку ниже';

        }

        $modelStatus->save();

        return $this->render('@frontend/themes/service/modules/core/views/index/inform', ['text' => $text]);


    }

    public function actionItem($id)
    {
        $userId = Yii::$app->user->identity->getId();

        $model = Section::find()
                            ->where(['id' => $id])
                            ->andWhere(['active' => '1'])
                            ->one();

        if (!$model) {
            throw new NotFoundHttpException('Section not found');
        }

        $modelStatus  = LearnStatus::find()
                                ->where(['user_id' => $userId])
                                ->one();

        if ($modelStatus->block_route) {
            return $this->redirect([$modelStatus->block_route]);
        }

        if ($modelStatus->generateVerifyMd5() !== $model->generateVerifyMd5()) {

            $this->layout = '@app/views/layouts/main';
            return $this->render('@frontend/themes/service/modules/core/views/index/danied');
        }

        if ($part = $model->getFirstPart()) {
            $modelStatus->part_id = $part;
            $modelStatus->clearFields(['task_section_id', 'task_part_id', 'status', 'block_data', 'block_route']);
        } else {
            $modelStatus->clearFields(['task_section_id', 'part_id', 'section_id', 'task_part_id', 'status', 'block_data', 'block_route']);
            $modelStatus->status = 'finish';
        }
        $modelStatus->save();

        return $this->redirect(['/core/index/index']);
    }

    public function actionChooseTime()
    {
        $userId = Yii::$app->user->identity->getId();

        $modelStatus  = LearnStatus::find()
                                      ->where(['user_id' => $userId])
                                      ->one();

        if ($modelStatus->block_route !== '/test/section/choose-time') {
            throw new NotFoundHttpException('404 not found');
        }

        $section = Section::find()
                            ->where(['id' => $modelStatus->section_id])
                            ->andWhere(['active' => '1'])
                            ->one();

        $learnTime = LearnTime::find()
                                  ->where(['user_id' => $userId])
                                  ->andWhere(['section_id' => $section->id])
                                  ->one();

        if ($learnTime && $learnTime->is_send_delay) {
            $text = $this->renderPartial('delay-learn', ['section' => $section]);
            $delay = TRUE;
        } else {
            $text = $this->renderPartial('empty-learn', ['section' => $section]);
            $delay = FALSE;
        }

        $this->layout = '@app/views/layouts/main';

        return $this->render('choose-time', [
            'section' => $section,
            'text'    => $text,
            'delay'   => $delay,
        ]);
    }

    public function actionSetTime()
    {
        $sectionId  = Yii::$app->request->post('section_id');
        $time       = Yii::$app->request->post('time');
        $userId     = Yii::$app->user->identity->getId();
        $timestamp  = strtotime($time) + 3600 * 24;

        if ($timestamp < time()) {
            return $this->redirect(['/test/section/choose-time', 'error' => ($time) ? 'Дата не может быть меньше текущей' : 'Не выбрана дата']);
        }

        $section = Section::find()
                                ->where(['id' => $sectionId])
                                ->andWhere(['active' => '1'])
                                ->one();

        $modelStatus  = LearnStatus::find()
                                      ->where(['user_id' => $userId])
                                      ->one();

        if ($modelStatus->block_route !== '/test/section/choose-time') {
            throw new NotFoundHttpException('404 not found');
        }

        $currentSection = Section::findOne($modelStatus->section_id);

        $listLearnSections = ArrayHelper::map(Section::find()->where(['<=', 'display_order', $currentSection->display_order])->all(), 'id', 'id');
        
        if ($section && isset($listLearnSections[ $sectionId ]) && $time) {
            
            $learnTime = LearnTime::find()
                                  ->where(['user_id' => $userId])
                                  ->andWhere(['section_id' => $sectionId])
                                  ->one();

            if (!$learnTime) {
                
                $learnTime              = new LearnTime();
                $learnTime->user_id     = $userId;
                $learnTime->section_id  = $sectionId;

            }

            $learnTime->time_finish   = $timestamp;
            $learnTime->is_send_delay = 0;

            $learnTime->save();

            $modelStatus->clearFields(['block_route', 'block_data']);
            $modelStatus->save();
        }

        return $this->redirect(['/core/index/index']);
    }

}
?>