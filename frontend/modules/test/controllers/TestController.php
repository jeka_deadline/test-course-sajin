<?php
namespace frontend\modules\test\controllers;

use Yii;
use yii\web\Controller;
use frontend\modules\test\models\Test;
use yii\web\NotFoundHttpException;
use frontend\modules\user\models\LearnStatus;
use yii\filters\AccessControl;
use frontend\modules\test\models\LogTest;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use frontend\modules\test\models\Question;

class TestController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['item', 'answer', 'repeat-parts', 'agree-repeat-part'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionItem($id)
    {
        $this->layout = '@app/views/layouts/main';
        $userId = Yii::$app->user->identity->getId();

        $model = Test::find()
                          ->where(['id' => $id])
                          ->andWhere(['active' => '1'])
                          ->one();

        if (!$model) {
            throw new NotFoundHttpException('Test not found');
        }

        $modelStatus  = LearnStatus::find()
                                      ->where(['user_id' => $userId])
                                      ->one();

        if ($modelStatus->block_route) {
            return $this->redirect([$modelStatus->block_route]);
        }

        if (!$modelStatus) {
            return $this->redirect(['/core/index/index']);
        }

        if ($modelStatus->generateVerifyMd5() !== $model->generateVerifyMd5() || $modelStatus->block_route) {

            $this->layout = '@app/views/layouts/main';
            return $this->render('@frontend/themes/service/modules/core/views/index/danied');
        }

        return $this->render('item', [
            'testId' => $id,
        ]);
    }

    public function actionAnswer()
    {
        $userId = Yii::$app->user->identity->getId();

        $modelStatus  = LearnStatus::find()
                                      ->where(['user_id' => $userId])
                                      ->one();

        if ($modelStatus->block_route) {
            return $this->redirect([$modelStatus->block_route]);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        $wrongQuestionsId           = [];
        $session                    = Yii::$app->session;

        $response = ['status' => 'error', 'dataErrors' => [], 'text' => '', 'repeat' => ''];
        $testId   = Yii::$app->request->post('test_id');

        $test     = Test::find()
                            ->where(['id' => $testId])
                            ->andWhere(['active' => '1'])
                            ->one();

        if (!$test) {
            $response[ 'text' ] = 'Не корректные данные';
            return $response;
        }

        if ($modelStatus->generateVerifyMd5() !== $test->generateVerifyMd5()) {
            $response[ 'text' ] = 'Вам запрещен доступ к этому тесту';
            return $response;
        }

        $logTest        = LogTest::find()
                                      ->where(['user_id' => '1'])
                                      ->andWhere(['test_id' => $testId])
                                      ->one();

        if (!$logTest) {
            $logTest                  = new LogTest();
            $logTest->user_id         = 1;
            $logTest->test_id         = $testId;
            $logTest->count_attempts  = 0;
        }

        $query = $test->getQuestions()->where(['active' => '1']);

        if ($session->has('wrongQuestionsId')) {
            $query->andWhere(['in', 'id', $session->get('wrongQuestionsId')]);
        }

        $questionsList  = ArrayHelper::map($query->all(), 'id', function($model){return $model;});

        $postAnswer = Yii::$app->request->post('Answer');

        if (!$postAnswer) {
            foreach ($questionsList as $question) {
                $response[ 'dataErrors' ][] = ['question_id' => $question->id, 'text' => 'Вы не дали ответ на этот вопрос'];
            }

            $logTest->count_attempts++;
            $logTest->status = 'wrong';
            $logTest->save();

            return $response;
        }

        foreach ($questionsList as $question) {
            $answer = (isset($postAnswer[ $question->id ])) ? $postAnswer[ $question->id ] : NULL;

            if (!$answer) {
                $response[ 'dataErrors' ][] = ['question_id' => $question->id, 'text' => 'Вы не дали ответ на этот вопрос']; 
                continue; 
            }
            if (!$question->isCorrectAnswer($postAnswer[ $question->id ])) {
                $response[ 'dataErrors' ][] = ['question_id' => $question->id, 'text' => 'Вы неправильно ответили на этот вопрос'];
                $wrongQuestionsId[] = $question->id;
                continue; 
            }
        }

        if (empty($response[ 'dataErrors' ])) {

            if ($session->has('wrongQuestionsId')) {
                unset($session[ 'wrongQuestionsId' ]);
            }
            $response[ 'status' ] = 'success';
            $response[ 'text' ] = $this->renderPartial('@frontend/themes/service/modules/core/views/index/inform', ['text' => 'Вы успешно прошли тест, для перехода к следующему пункту нажмите кнопку ниже']);
            $logTest->count_attempts++;
            $logTest->status = 'ok';
            if ($taskSection = $test->getFirstSectionTask()) {
                $modelStatus->clearFields(['part_id', 'test_id', 'task_part_id', 'status', 'block_data', 'block_route']);
                $modelStatus->task_section_id = $taskSection;
            } else {
                if ($nextSection = $test->getNextSection()) {
                    $modelStatus->section_id = $nextSection;
                    $modelStatus->clearFields(['part_id', 'test_id', 'task_part_id', 'task_section_id', 'status', 'block_data', 'block_route']);
                } else {
                    $modelStatus->status = 'finish';
                    $modelStatus->clearFields(['part_id', 'test_id', 'task_part_id', 'task_section_id', 'section_id', 'block_data', 'block_route']);
                }

            }

            $modelStatus->save();

        } else {
            $modelStatus->block_route = '/test/test/repeat-parts';
            $modelStatus->block_data  = serialize($wrongQuestionsId);
            $response[ 'text' ]       = $this->renderPartial('html-repeat');

            $modelStatus->save();

            if ($wrongQuestionsId) {
                $session->set('wrongQuestionsId', $wrongQuestionsId);
                $response[ 'repeat' ] = TRUE;
            }

            $logTest->count_attempts++;
            $logTest->status = 'wrong';
            
        }

        $logTest->save();

        return $response;
    }

    // метод для повторения частей
    public function actionRepeatParts()
    {
        $userId = Yii::$app->user->identity->getId();

        $modelStatus  = LearnStatus::find()
                                      ->where(['user_id' => $userId])
                                      ->one();

        if ($modelStatus->block_route !== '/test/test/repeat-parts') {
            throw new NotFoundHttpException('404 not found');
        }

        $data = unserialize($modelStatus->block_data);

        if (empty($data)) {
            $modelStatus->clearFields(['block_route', 'block_data']);
            $modelStatus->save();

            return $this->redirect(['/core/index/index']);
        }

        $questionsList = Question::find()->where(['in', 'id', $data])->all();

        $this->layout = '@app/views/layouts/main';
        return $this->render('repeat-part', ['questionsList' => $questionsList]);

    }

    public function actionAgreeRepeatPart()
    {
        $agree  = Yii::$app->request->post('agree');
        $userId = Yii::$app->user->identity->getId();

        if ($agree) {

            $modelStatus  = LearnStatus::find()
                                          ->where(['user_id' => $userId])
                                          ->one();

            $modelStatus->clearFields(['block_data', 'block_route']);
            $modelStatus->save();

            $this->layout = '@app/views/layouts/main';
            return $this->render('@frontend/themes/service/modules/core/views/index/inform', ['text' => 'Вы узнали все ответы на неправильные вопросы, для перехода к тесту нажмите кнопку ниже']);

        } else {
            return $this->redirect(['/test/test/repeat-parts', 'error' => 'Нужно подтвердить прочитанное']);
        }

    }

}

?>