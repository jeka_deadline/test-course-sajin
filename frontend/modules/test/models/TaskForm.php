<?php
namespace frontend\modules\test\models;

use Yii;
use yii\base\Model;
use frontend\modules\test\models\Task;
use yii\web\NotFoundHttpException;
use frontend\modules\user\models\User;
use frontend\modules\test\models\TaskLog;

class TaskForm extends Model
{

    public $text;
    public $type;
    public $taskId;

    public function rules()
    {
        return [
            [['text', 'type', 'taskId'], 'required', 'message' => '{attribute} не заполнено'],
            [['text'], 'string'],
        ];
    }

    public function sendMessage()
    {
        $task = Task::find()
                        ->where(['active' => '1'])
                        ->andWhere(['id' => $this->taskId])
                        ->one();

        if (!$task) {
            throw new NotFoundHttpException('Task not found');
        }

        $user = User::findOne(Yii::$app->user->identity->getId());

        $taskLog = TaskLog::find()
                              ->where(['task_id' => $task->id])
                              ->andWhere(['user_id' => $user->id])
                              ->one();

        if (!$taskLog) {

            $taskLog          = new TaskLog();
            $taskLog->task_id = $task->id;
            $taskLog->user_id = $user->id;
        
        }

        $taskLog->status      = 'new';
        $taskLog->text_answer = $this->text;

        $taskLog->save();

        $headers    = 'MIME-Version: 1.0' . "\r\n";
        $headers    .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers    .= 'From: course@vlu.su';

        $text       = '<b>1. Название курса: </b>Сообщение с курса "RECRU-TEC"<br>';
        $text .= '<b>2. ФИО пользователя: </b>' . $user->surname . ' ' . $user->name . ' (id = ' . $user->id . ')<br>';
        $text .= '<b>3. Email: </b>' . $user->email . '<br>';
        $text .= '<b>4. Текст задания:</b><br>' . $task->text . '<br>';
        $text .= '<b>5. Текст выполненого задания</b><br>' . $this->text . '<br>';
        $text .= '<br><br><hr color="#800000" align="left" width="95%"" size="7px"><br><br><a href="http://vlu.su/"" target="_blank"><img width="140" height="76" alt="" src="http://nofailhiring.ru/promotion/vlu.png"></a><br><br> С уважением, команда «Высшая Лига Управления» <br><br> Тел. +7 (965) 240 49 90<br><br> Тел. +7 926 700 82 71<br><br>Техподдержка: info@vlu.su'; 

        $subject    = '(Курс recru_tec) Задание от пользователя ' . $user->surname . ' ' . $user->name;

        return mail(Yii::$app->params[ 'adminEmail' ], $subject, $text, $headers);
    }

}