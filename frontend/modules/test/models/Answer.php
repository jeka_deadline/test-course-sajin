<?php

namespace frontend\modules\test\models;

use Yii;
use common\models\test\Answer as BaseAnswer;

class Answer extends BaseAnswer
{

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Question::className(), ['id' => 'question_id']);
    }

}
