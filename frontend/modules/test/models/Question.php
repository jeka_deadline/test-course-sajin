<?php

namespace frontend\modules\test\models;

use Yii;
use common\models\test\Question as BaseQuestion;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

class Question extends BaseQuestion
{

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestLinksTestsQuestions()
    {
        return $this->hasMany(LinksTestsQuestions::className(), ['question_id' => 'id']);
    }

    public function getAnswers()
    {
        return $this->hasMany(Answer::className(), ['question_id' => 'id']);
    }

    public function getPart()
    {
        return $this->hasOne(Part::className(), ['id' => 'correct_answer_part_id']);
    }

    public function getAnswersList()
    {
        return ArrayHelper::map($this->getAnswers()->where(['active' => '1'])->orderBy('rand()')->all(), 'id', 'text');
    }

    public function isCorrectAnswer($dataAnswers)
    {
        if (empty($dataAnswers)) {
            return FALSE;
        }

        // обработчик для одного правильного ответа
        if ($this->type === 'one') {
            
            $answer = $this->getAnswers()->where(['active' => '1'])->andWhere(['type' => 'correct'])->one();

            if (!$answer || $answer->id != $dataAnswers) {
                return FALSE;
            }

            return TRUE;
        }

        // обработчик для множества правильных ответов
    }

}
