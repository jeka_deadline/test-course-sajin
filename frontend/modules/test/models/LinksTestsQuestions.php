<?php

namespace frontend\modules\test\models;

use common\models\test\LinksTestsQuestions as BaseLinksTestsQuestions;

class LinksTestsQuestions extends BaseLinksTestsQuestions
{
 
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTest()
    {
        return $this->hasOne(Test::className(), ['id' => 'test_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Question::className(), ['id' => 'question_id']);
    }

}
