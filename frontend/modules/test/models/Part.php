<?php

namespace frontend\modules\test\models;

use Yii;
use common\models\test\Part as BasePart;
use yii\helpers\ArrayHelper;
use frontend\modules\user\models\LearnStatus;

class Part extends BasePart
{

    public function generateVerifyMd5()
    {
        $section = $this->section;
        if ($section) {
            return md5('section_id' . $section->id . 'part_id' . $this->id . 'test_id' . NULL . 'task_section_id' . NULL . 'task_part_id' . NULL . 'status' . NULL);
        }

        return NULL;
    }

    public function getListTasks()
    {
        return $this->getTasks()
                            ->where(['active' => '1'])
                            ->orderBy(['display_order' => SORT_ASC])
                            ->all();
        

    }

    public function getListTasksForSection()
    {
        $section = $this->section;

        if ($section) {
            return $section->getTasks()
                                  ->where(['active' => '1'])
                                  ->orderBy(['display_order' => SORT_ASC])
                                  ->all();
        }

        return NULL;
    }

    public function isLastPart()
    {
        $lastPart = $this->section->getParts()
                                      ->where(['active' => '1'])
                                      ->andWhere(['>', 'display_order', $this->display_order])
                                      ->one();

        return ($lastPart) ? FALSE : TRUE;
    }

    public function getTest()
    {
        $section = $this->section;

        if ($section && $section->test) {
            return $section->test->id;
        }

        return NULL;
    }

    public function getStatusForSidebar(LearnStatus $status)
    {
        if ($status->generateVerifyMd5() === $this->generateVerifyMd5()) {
            return 'active';
        }

        if ($this->id === $status->part_id) {
            return NULL;
        }
        
        if ($status->part_id === NULL) {
            $section = $this->section;
            //throw new Exception("Error Processing Request", 1);
            

            return ($status->section_id > $section->id || $status->section_id === $section->id || ($status->status && $status->status !== 'start')) ? 'learn' : 'block';
        }

        $part = self::findOne($status->part_id);

        $learnParts = ArrayHelper::map(self::find()->where(['<', 'display_order', $part->display_order])->andWhere(['section_id' => $status->section_id])->all(), 'id', 'id');


        $section = Section::findOne($status->section_id);

        if ($section) {

            $learnSections = ArrayHelper::map(Section::find()->where(['<', 'display_order', $section->display_order])->all(), 'id', function($model){return $model;});

            foreach ($learnSections as $section) {
                $learnParts = ArrayHelper::merge($learnParts, ArrayHelper::map($section->parts, 'id', 'id'));
            }
        }

        return (isset($learnParts[ $this->id ]) || ($status->status && $status->status !== 'start')) ? 'learn' : 'block';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSection()
    {
        return $this->hasOne(Section::className(), ['id' => 'section_id']);
    }

    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['part_id' => 'id']);
    }

}
