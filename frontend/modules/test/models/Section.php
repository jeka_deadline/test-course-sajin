<?php

namespace frontend\modules\test\models;

use Yii;
use common\models\test\Section as BaseSection;
use frontend\modules\user\models\LearnStatus;
use yii\helpers\ArrayHelper;

class Section extends BaseSection
{

    public function getNextPart($currentPart = NULL)
    {
        if ($currentPart) {

        } else {
          
        }
    }

    public function generateVerifyMd5()
    {
        return md5('section_id' . $this->id . 'part_id' . NULL . 'test_id' . NULL . 'task_section_id' . NULL . 'task_part_id' . NULL . 'status' . NULL);
    }

    public function getFirstPart()
    {
        $firstPart = $this->getParts()->where(['active' => '1'])->orderBy(['display_order' => SORT_ASC])->one();

        if ($firstPart) {
            return $firstPart->id;
        }

        return NULL;
    }

    public function getLastPart()
    {
        $lastPart = $this->getParts()->where(['active' => '1'])->orderBy(['display_order' => SORT_DESC])->one();

        if ($lastPart) {
            return $lastPart->id;
        }

        return NULL;
    }

    public function getStatusForSidebar(LearnStatus $status)
    {
        if ($status->generateVerifyMd5() === $this->generateVerifyMd5()) {
            return 'active';
        }

        if ($status->status && $status->status !== 'start') {
            return 'learn';
        }

        if ($this->id === $status->section_id) {
            return NULL;
        }

        $section = self::findOne($status->section_id);

        if ($section) {

            $learnSections = ArrayHelper::map(self::find()->where(['<', 'display_order', $section->display_order])->all(), 'id', 'id');

            return (isset($learnSections[ $this->id ]) || ($status->status && $status->status !== 'start')) ? 'learn' : 'block';
        }

        return 'block';
    }

    public function getParts()
    {
        return $this->hasMany(Part::className(), ['section_id' => 'id']);
    }

    public function getTest()
    {
        return $this->hasOne(Test::className(), ['section_id' => 'id']);
    }

    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['section_id' => 'id']);
    }

}
