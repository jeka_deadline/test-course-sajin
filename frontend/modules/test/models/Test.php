<?php
namespace frontend\modules\test\models;

use Yii;
use common\models\test\Test as BaseTest;
use frontend\modules\test\models\Section;
use frontend\modules\user\models\LearnStatus;
use yii\helpers\ArrayHelper;

class Test extends BaseTest
{

    public function generateVerifyMd5()
    {
        $section = $this->section;

        if ($section) {
            return md5('section_id' . $section->id . 'part_id' . NULL .  'test_id' . $this->id . 'task_section_id' . NULL . 'task_part_id' . NULL . 'status' . NULL);
        }

        return NULL;
    }

    public function getNextSection()
    {
        $section = $this->section;

        if ($section) {
            $section = Section::find()
                                  ->where(['active' => '1'])
                                  ->andWhere(['>', 'display_order', $section->display_order])
                                  ->one();

            if ($section) {
                return $section->id;
            }
        }

        return NULL;
    }

    public function getFirstSectionTask()
    {
        $section = $this->section;

        if ($section) {
            $task = $section->getTasks()
                                  ->where(['active' => '1'])
                                  ->orderBy(['display_order' => SORT_ASC])
                                  ->one();

            if ($task) {
                return $task->id;
            }
        }

        return NULL;
    }

    public function getStatusForSidebar(LearnStatus $status)
    {
        if ($status->generateVerifyMd5() === $this->generateVerifyMd5()) {
            return 'active';
        }

        if ($this->section->id === $status->section_id && $status->task_section_id) {
            return 'learn';
        }

        $section = Section::findOne($status->section_id);

        if ($section) {

            $learnSections = ArrayHelper::map(Section::find()->where(['<', 'display_order', $section->display_order])->all(), 'id', 'id');

            return (isset($learnSections[ $this->section_id ]) || ($status->status && $status->status !== 'start')) ? 'learn': 'block';
        }

        return 'block';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSection()
    {
        return $this->hasOne(Section::className(), ['id' => 'section_id']);
    }

    public function getQuestions()
    {
        return $this->hasMany(Question::className(), ['id' => 'question_id'])->viaTable(LinksTestsQuestions::tableName(), ['test_id' => 'id']);
    }

}
