<?php

namespace frontend\modules\test\models;

use Yii;
use common\models\test\Task as BaseTask;
use frontend\modules\user\models\LearnStatus;

class Task extends BaseTask
{

    public function generateVerifyMd5()
    {
        if ($this->section) {
            $section          = $this->section->id;
            $taskSectionId    = $this->id;
            $part             = NULL;
            $taskPartId       = NULL;
        }
        if ($this->part) {

            $section        = $this->part->section->id;
            $taskSectionId  = NULL;
            $part           = $this->part->id;
            $taskPartId     = $this->id;
        
        }

        return md5('section_id' . $section . 'part_id' . $part . 'test_id' . NULL .  'task_section_id' . $taskSectionId . 'task_part_id' . $taskPartId . 'status' . NULL);
    }

    public function getNextPartTask()
    {
        $task = $this->part->getTasks()
                                  ->where(['>', 'display_order', $this->display_order])
                                  ->andWhere(['active' => '1'])
                                  ->orderBy(['display_order' => SORT_ASC])
                                  ->one();

        if ($task) {
            return $task->id;
        }

        return NULL;

    }

    public function getSectionTaks()
    {
        $part = $this->part;

        if ($part) {
            $section = $part->section;
        } else {
            $section = $this->section;
        }

        if (!$section) {
            return FALSE;
        }

        $task = $section->getTasks()->where(['active' => '1'])->orderBy(['display_order' => SORT_ASC])->one();

        if ($task) {
            return $task->id;
        }

        return NULL;
    }

    public function getTest()
    {
        $part = $this->part;

        if ($part) {
            $section = $part->section;
        } else {
            $section = $this->section;
        }

        if (!$section) {
            return FALSE;
        }

        $test = $section->getTest()->where(['active' => '1'])->one();

        if ($test) {
            return $test->id;
        }

        return NULL;
    }

    public function getNextSectionTask()
    {
        $model = NULL;

        if ($this->part) {
            $model = $this->part->section;
        }

        if ($this->section) {
            $model = $this->section;
        }

        if (!$model) {
            return NULL;
        }

        $task = $model->getTasks()
                            ->where(['>', 'display_order', $this->display_order])
                            ->andWhere(['active' => '1'])
                            ->orderBy(['display_order' => SORT_ASC])
                            ->one();

        if ($task) {
            return $task->id;
        }

        return NULL;
    }

    public function getNextPart()
    {
        $part = $this->part;

        if (!$part) {
            return NULL;
        }

        $section = $part->section;

        if (!$section) {
            return NULL;
        }

        $part = Part::find()
                        ->where(['active' => '1'])
                        ->andWhere(['>', 'display_order', $part->display_order])
                        ->andWhere(['section_id' => $section->id])
                        ->orderBy(['display_order' => SORT_ASC])
                        ->one();

        if ($part) {
            return $part->id;
        }

        return NULL;
    }

    public function getStatusForSidebar(LearnStatus $status)
    {

        if ($status->task_part_id === $this->id || $status->task_section_id === $this->id) {
            return 'active';
        }

        $taskLog = TaskLog::find()
                              ->where(['user_id' => Yii::$app->user->identity->getId()])
                              ->andWhere(['task_id' => $this->id])
                              ->one();

        if ($taskLog) {
            switch ($taskLog->status) {
                case 'new':
                    return 'new';
                case 'correct';
                    return 'learn';
                case 'wrong':
                    return 'wrong';
                case 'rework':
                    return 'rework';
            }
        }

        if ($status->status) {
            return '';
        }

        return 'block';
    }

    public function getNextSection()
    {
        $section = NULL;

        if ($this->part) {
            $section = $this->part->section;
        }

        if ($this->section) {
            $section = $this->section;
        }

        if (!$section) {
            return NULL;
        }

        $section = Section::find()
                              ->where(['active' => '1'])
                              ->andWhere(['>', 'display_order', $section->display_order])
                              ->orderBy(['display_order' => SORT_ASC])
                              ->one();

        if ($section) {
            return $section->id;
        }

        return NULL;
    }

    public function getSection()
    {
        return $this->hasOne(Section::className(), ['id' => 'section_id']);
    }

    public function getPart()
    {
        return $this->hasOne(Part::className(), ['id' => 'part_id']);
    }

}
