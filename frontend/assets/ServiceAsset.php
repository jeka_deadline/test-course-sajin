<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ServiceAsset extends AssetBundle
{

    public $sourcePath = '@app/themes/service/assets';

    /*public $basePath = '@webroot';
    public $baseUrl = '@web';*/
    public $css = [
        'css/style.css',
    ];
    public $js = [
        'js/frontend.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];

    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];
}