<?php
    return [
        'core' => [
            'class' => 'frontend\modules\core\Module',
        ],
        'user' => [
            'class' => 'frontend\modules\user\Module',
        ],
        'test' => [
            'class' => 'frontend\modules\test\Module',
        ],
    ];
?>