<?php

namespace common\models\test;

use Yii;

/**
 * This is the model class for table "test_questions".
 *
 * @property integer $id
 * @property string $type
 * @property string $correct_answer
 * @property string $name
 * @property string $title
 * @property integer $display_order
 * @property string $active
 */
class Question extends \yii\db\ActiveRecord
{
    public $filePath = 'files/test/question';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_questions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'active'], 'string'],
            [['correct_answer', 'name', 'title'], 'required'],
            [['display_order'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['title', 'correct_answer'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => 'ID',
            'type'            => 'Тип вопроса',
            'correct_answer'  => 'Правильный ответ',
            'name'            => 'Название вопроса',
            'title'           => 'Заголовок вопроса',
            'display_order'   => 'Порядок отображения',
            'active'          => 'Активно',
        ];
    }
}
