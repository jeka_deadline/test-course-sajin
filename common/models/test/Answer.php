<?php

namespace common\models\test;

use Yii;

/**
 * This is the model class for table "test_answers".
 *
 * @property integer $id
 * @property integer $question_id
 * @property string $type
 * @property string $text
 * @property integer $display_order
 * @property string $active
 */
class Answer extends \yii\db\ActiveRecord
{
    public $filePath = 'files/test/answer';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_answers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question_id', 'type', 'text'], 'required'],
            [['question_id', 'display_order'], 'integer'],
            [['type', 'text', 'active'], 'string'],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => Question::className(), 'targetAttribute' => ['question_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question_id' => 'Вопрос',
            'type' => 'Тип ответа',
            'text' => 'Текст ответа',
            'display_order' => 'Порядок отображения',
            'active' => 'Активно',
        ];
    }
}
