<?php

namespace common\models\test;

use Yii;

/**
 * This is the model class for table "test_tasks".
 *
 * @property integer $id
 * @property integer $section_id
 * @property integer $part_id
 * @property string $title
 * @property string $text
 * @property integer $display_order
 * @property string $active
 */
class Task extends \yii\db\ActiveRecord
{
    public $filePath = 'files/test/task';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_tasks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['section_id', 'part_id', 'display_order'], 'integer'],
            [['title', 'text', 'display_order'], 'required'],
            [['text', 'active'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['display_order'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'section_id' => 'Глава к которой относится задание',
            'part_id' => 'Раздел к которому относится задание',
            'title' => 'Заголовок задания',
            'text' => 'Текст задания',
            'display_order' => 'Порядок отображения',
            'active' => 'Активно',
        ];
    }
}
