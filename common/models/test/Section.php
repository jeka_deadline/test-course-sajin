<?php

namespace common\models\test;

use Yii;

/**
 * This is the model class for table "test_section".
 *
 * @property integer $id
 * @property string $name
 * @property string $title_time
 * @property integer $display_order
 * @property string $active
 */
class Section extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_section';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'display_order'], 'required'],
            [['title_time', 'active'], 'string'],
            [['display_order'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['display_order'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название главы',
            'title_time' => 'Текст для календаря',
            'display_order' => 'Порядок отображения',
            'active' => 'Активно',
        ];
    }
}
