<?php

namespace common\models\test;

use Yii;

/**
 * This is the model class for table "test_links_tests_questions".
 *
 * @property integer $id
 * @property integer $test_id
 * @property integer $question_id
 *
 * @property TestTest $test
 * @property TestQuestions $question
 */
class LinksTestsQuestions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_links_tests_questions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['test_id', 'question_id'], 'required'],
            [['test_id', 'question_id'], 'integer'],
            [['test_id'], 'exist', 'skipOnError' => true, 'targetClass' => Test::className(), 'targetAttribute' => ['test_id' => 'id']],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => Question::className(), 'targetAttribute' => ['question_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'test_id' => 'Test ID',
            'question_id' => 'Question ID',
        ];
    }
}
