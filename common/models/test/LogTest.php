<?php

namespace common\models\test;

use Yii;
use common\models\user\User;

/**
 * This is the model class for table "test_log_test".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $test_id
 * @property integer $count_attempts
 * @property string $status
 */
class LogTest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_log_test';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'test_id', 'count_attempts', 'status'], 'required'],
            [['user_id', 'test_id', 'count_attempts'], 'integer'],
            [['status'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['test_id'], 'exist', 'skipOnError' => true, 'targetClass' => Test::className(), 'targetAttribute' => ['test_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'test_id' => 'Test ID',
            'count_attempts' => 'Count Attempts',
            'status' => 'Status',
        ];
    }
}
