<?php

namespace common\models\test;

use Yii;

/**
 * This is the model class for table "test_parts".
 *
 * @property integer $id
 * @property integer $section_id
 * @property string $title
 * @property string $text
 * @property integer $display_order
 * @property string $active
 *
 * @property TestSection $section
 */
class Part extends \yii\db\ActiveRecord
{

    public $filePath = 'files/test/part';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_parts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['section_id', 'display_order'], 'integer'],
            [['title', 'text', 'display_order'], 'required'],
            [['text', 'active'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['section_id'], 'exist', 'skipOnError' => true, 'targetClass' => Section::className(), 'targetAttribute' => ['section_id' => 'id']],
            [['display_order'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'section_id' => 'Глава',
            'title' => 'Заголовок раздела',
            'text' => 'Текст раздела',
            'display_order' => 'Порядок отображения',
            'active' => 'Активно',
        ];
    }
}
