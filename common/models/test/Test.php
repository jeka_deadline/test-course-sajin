<?php

namespace common\models\test;

use Yii;

/**
 * This is the model class for table "test_test".
 *
 * @property integer $id
 * @property integer $section_id
 * @property string $name
 * @property string $description
 * @property integer $display_order
 * @property string $active
 */
class Test extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_test';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['section_id', 'display_order'], 'integer'],
            [['name', 'description'], 'required'],
            [['description', 'active'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['section_id'], 'exist', 'skipOnError' => true, 'targetClass' => Section::className(), 'targetAttribute' => ['section_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'section_id' => 'Глава',
            'name' => 'Название теста',
            'description' => 'Описание теста',
            'display_order' => 'Порядок отображения',
            'active' => 'Активно',
        ];
    }
}
