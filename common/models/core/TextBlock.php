<?php

namespace common\models\core;

use Yii;

/**
 * This is the model class for table "core_text_block".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $uri
 * @property string $content
 * @property integer $display_order
 * @property integer $active
 */
class TextBlock extends \yii\db\ActiveRecord
{
    public $filePath = 'files/core/text-block';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_text_block';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'uri'], 'required'],
            [['content'], 'string'],
            [['display_order', 'active'], 'integer'],
            [['title'], 'string', 'max' => 100],
            [['description', 'uri'], 'string', 'max' => 255],
            [['uri'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'title'         => 'Заголовок',
            'description'   => 'Описание',
            'uri'           => 'Uri',
            'content'       => 'Контент',
            'display_order' => 'Плрядок отображения',
            'active'        => 'Активно',
        ];
    }
}
