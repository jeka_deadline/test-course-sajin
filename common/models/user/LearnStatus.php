<?php

namespace common\models\user;

use Yii;
use common\models\test\Section;
use common\models\test\Part;
use common\models\test\Test;
use common\models\test\Task;

/**
 * This is the model class for table "user_learn_status".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $section_id
 * @property integer $part_id
 * @property integer $test_id
 * @property integer $task_section_id
 * @property integer $task_part_id
 * @property string $status
 * @property string $block_route
 * @property string $block_data
 */
class LearnStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_learn_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'section_id', 'part_id', 'test_id', 'task_section_id', 'task_part_id'], 'integer'],
            [['status', 'block_data'], 'string'],
            [['block_route'], 'string', 'max' => 50],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['section_id'], 'exist', 'skipOnError' => true, 'targetClass' => Section::className(), 'targetAttribute' => ['section_id' => 'id']],
            [['part_id'], 'exist', 'skipOnError' => true, 'targetClass' => Part::className(), 'targetAttribute' => ['part_id' => 'id']],
            [['test_id'], 'exist', 'skipOnError' => true, 'targetClass' => Test::className(), 'targetAttribute' => ['test_id' => 'id']],
            [['task_section_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_section_id' => 'id']],
            [['task_part_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_part_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'section_id' => 'Section ID',
            'part_id' => 'Part ID',
            'test_id' => 'Test ID',
            'task_section_id' => 'Task Section ID',
            'task_part_id' => 'Task Part ID',
            'status' => 'Status',
            'block_route' => 'Block Route',
            'block_data' => 'Block Data',
        ];
    }
}
