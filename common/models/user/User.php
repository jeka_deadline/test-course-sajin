<?php

namespace common\models\user;

use Yii;

/**
 * This is the model class for table "user_users".
 *
 * @property integer $id
 * @property string $email
 * @property string $surname
 * @property string $name
 * @property string $login
 * @property string $password_hash
 * @property integer $blocked
 * @property integer $role
 * @property integer $first_enter
 * @property integer $last_enter
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'login', 'password_hash', 'role'], 'required'],
            [['blocked', 'role', 'first_enter', 'last_enter'], 'integer'],
            [['email'], 'string', 'max' => 100],
            [['surname', 'name', 'password_hash'], 'string', 'max' => 255],
            [['login'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'surname' => 'Surname',
            'name' => 'Name',
            'login' => 'Login',
            'password_hash' => 'Password Hash',
            'blocked' => 'Blocked',
            'role' => 'Role',
            'first_enter' => 'First Enter',
            'last_enter' => 'Last Enter',
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return NULL;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return NULL;
    }

    public function validateAuthKey($authKey)
    {
        return NULL;
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
}
