<?php

namespace common\models\user;

use Yii;
use common\models\test\Section;

/**
 * This is the model class for table "user_learn_time".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $section_id
 * @property integer $time_finish
 * @property integer $is_send_delay
 */
class LearnTime extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_learn_time';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'section_id', 'time_finish'], 'required'],
            [['user_id', 'section_id', 'is_send_delay'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['section_id'], 'exist', 'skipOnError' => true, 'targetClass' => Section::className(), 'targetAttribute' => ['section_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'section_id' => 'Section ID',
            'time_finish' => 'Time Finish',
            'is_send_delay' => 'Is Send Delay',
        ];
    }
}
