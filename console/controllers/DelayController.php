<?php
namespace console\controllers;

use yii\console\Controller;
use console\models\user\LearnStatus;
use console\models\user\LearnTime;

class DelayController extends Controller
{

    public function actionDelay()
    {
        $learnStatus = LearnStatus::find()->all();

        foreach ($learnStatus as $userStatus) {
            $learnTime = LearnTime::find()
                                        ->where(['user_id' => $userStatus->user_id])
                                        ->andWhere(['section_id' => $userStatus->section_id])
                                        ->andWhere(['<', 'time_finish', time()])
                                        ->andWhere(['is_send_delay' => '0'])
                                        ->one();

            if ($learnTime) {
                $userStatus->block_route = '/test/section/choose-time';
                $userStatus->save();

                if ($learnTime->sendMessage()) {
                    $learnTime->updateAttributes(['is_send_delay' => '1']);
                }
            }
        }
    }

}