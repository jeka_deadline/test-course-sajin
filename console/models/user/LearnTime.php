<?php

namespace console\models\user;

use Yii;
use common\models\user\LearnTime as BaseLearnTime;
use common\models\test\Section;

class LearnTime extends BaseLearnTime
{

    public function sendMessage()
    {
        $user     = $this->user;
        $section  = $this->section;

        $subject = 'Просрочка главы пользователя по курсу recru_tec';
        $text    = 'Пользователь:<br><b>Фамилия:</b> ' . $user->surname . ',<br><b>Имя:</b> ' . $user->name . ',<br>Просрочил обучение главы ' . $section->name . ' (id = ' . $section->id . ')';
        $text   .= '<br><br><hr color="#800000" align="left" width="95%"" size="7px"><br><br><a href="http://vlu.su/"" target="_blank"><img width="140" height="76" alt="" src="http://nofailhiring.ru/promotion/vlu.png"></a><br><br> С уважением, команда «Высшая Лига Управления» <br><br> Тел. +7 (965) 240 49 90<br><br> Тел. +7 926 700 82 71<br><br>Техподдержка: info@vlu.su'; 
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= 'From: course@vlu.su';

        //print_r($text);
        return mail(Yii::$app->params[ 'adminEmail' ] . ',' . $user->email, $subject, $text, $headers);

    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getSection()
    {
        return $this->hasOne(Section::className(), ['id' => 'section_id']);
    }

}
